import React, { Component } from 'react'
import { Table, TableRow, TableCell, TablePagination, Typography, TableHead, TableBody, Card, TableFooter, } from '@material-ui/core';

import { host } from '../../socket';
import axios from 'axios';
import QuyTrinhView from '../../components/quyTrinhView/QuyTrinhView';
export default class QuyTrinhPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchForm: {
                bienSo: "",
                loaiquytrinh: -1,
                tuNgay: this.yyyymmdd((new Date((new Date()).setDate(new Date().getDate() - 30)))),
                denNgay: this.yyyymmdd(new Date()),
                page: 0,
                pageSize: 10,
                giaidoanhientai: -1,
            },
            pagination: {
                content: [],
                pageable:
                {
                    pageSize: 10,
                    pageNumber: 0,
                },
                numberOfElements: 0,
            },
            nguoiDungHienHanh: null,
        }
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        //this.handleInputChange = this.handleInputChange.bind(this);
        this.searchStringChange = this.searchStringChange.bind(this);
        this.getQuytrinhs = this.getQuytrinhs.bind(this);
        this.submit = this.submit.bind(this);
        this.tuNgayChange = this.tuNgayChange.bind(this);
        this.denNgayChange = this.denNgayChange.bind(this);
        this.loaiQuyTrinhChange = this.loaiQuyTrinhChange.bind(this);
        this.giaiDoanChange = this.giaiDoanChange.bind(this);
        this.giaiDoanSelection=this.giaiDoanSelection.bind(this);
        this.getQuytrinhs();
        this.getNguoiDungHienHanh();
    }
    getNguoiDungHienHanh() {
        axios.get(host + "/authen/lay-nguoi-dung-hien-hanh")
            .then(res => {
                this.setState({
                    nguoiDungHienHanh: res.data
                })
            });
    }
    yyyymmdd(x) {
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        var h = x.getHours().toString();
        var mm = x.getMinutes().toString();
        var ss = x.getSeconds().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        (h.length == 1) && (h = '0' + h);
        (mm.length == 1) && (mm = '0' + mm);
        (ss.length == 1) && (ss = '0' + ss);
        var yyyymmdd = y + "-" + m + "-" + d + "T" + h + ":" + mm + ":" + ss;
        return yyyymmdd;
    }
    getQuytrinhs() {
        var url = host + "/quyTrinh/lay-ds";
        axios.post(url,
            this.state.searchForm)
            .then((res) => {
                this.setState({
                    pagination: res.data
                })
            });
    }
    searchStringChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                bienSo: value,
            },
        }));
    }
    tuNgayChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                tuNgay: value,
            },
        }));
    }
    giaiDoanChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                giaidoanhientai: value,
            },
        }));
    }
    denNgayChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                denNgay: value,
            },
        }));
    }
    handleChangePage(event, newpage) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                page: newpage,
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageNumber: newpage,
                }
            },
        }), () => this.getQuytrinhs());
        //this.getXes(newpage, this.state.pagination.pageable.pageSize);
    }
    handleChangeRowsPerPage(event) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                pageSize: event.target.value
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageSize: event.target.value,
                }
            },
        }), () => this.getQuytrinhs());
        //this.getXes(this.state.pagination.pageable.pageNumber, event.target.value);

    }

    loaiQuyTrinhChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                loaiquytrinh: value,
            },
        }));
    }
    submit(e) {
        e.preventDefault();
        this.getQuytrinhs();
        return false;
    }
    giaiDoanSelection() {
        if (this.state.searchForm.loaiquytrinh == 1)
            return (
                <select onChange={this.giaiDoanChange} class="form-control" value={this.state.searchForm.giaidoanhientai} >
                    <option value="-1">Tất cả</option>
                    <option value="1">Đã cân lần 1</option>
                    <option value="2">Đã cân lần 2</option>
                </select>
            )

        if (this.state.searchForm.loaiquytrinh == 2)
            return (
                <select onChange={this.giaiDoanChange} class="form-control" value={this.state.searchForm.giaidoanhientai} >
                    <option value="-1">Tất cả</option>
                </select>
            );
        else
            return <select onChange={this.giaiDoanChange} class="form-control" value={this.state.searchForm.giaidoanhientai} >
                <option value="-1">Tất cả</option>
                <option value="1">Giai đoạn 1</option>
                <option value="2">Giai đoạn 2</option>
            </select>
    }
    getDsLoaiQuyTrinhView() {
        if (this.state.nguoiDungHienHanh == null)
            return "";
        if (this.state.nguoiDungHienHanh.idVaiTro == 3 || this.state.nguoiDungHienHanh.idVaiTro == 4)
            return <select onChange={this.loaiQuyTrinhChange} class="form-control" value={this.state.searchForm.loaiquytrinh} >
            <option value="1">Nhập rác</option>
            <option value="2">Chôn rác</option>
        </select>;
        return <select onChange={this.loaiQuyTrinhChange} class="form-control" value={this.state.searchForm.loaiquytrinh} >
        <option value="-1">Tất cả</option>
        <option value="1">Nhập rác</option>
        <option value="2">Chôn rác</option>
        <option value="3">Khác</option>
    </select>;
    }
    render() {
        return (
            <div style={{ padding: "10px" }}>
                <Card style={{ marginBottom: "10px", padding: "10px" }}>
                    <div className="container-fluid">
                        <form onSubmit={this.submit}>
                            <div className="row">
                                <div class="col-md-3">
                                    <label>Từ ngày</label>
                                    <input onChange={this.tuNgayChange} value={this.state.searchForm.tuNgay} placeholder="Từ ngày" className="form-control" type="datetime-local" />
                                </div>
                                <div className="col-md-3">
                                    <label>Đến ngày</label>
                                    <input onChange={this.denNgayChange} value={this.state.searchForm.denNgay} placeholder="Đến ngày" className="form-control" type="datetime-local" />
                                </div>
                                <div className="col-md-2">
                                    <label>Loại quy trình</label>
                                    {this.getDsLoaiQuyTrinhView()}
                                </div>
                                <div className="col-md-2">
                                    <label>Checkpoint</label>
                                    {this.giaiDoanSelection()}
                                </div>
                                <div className="col-md-2">
                                    <label>Biển số</label>
                                    <div class="input-group">
                                        <input value={this.state.searchForm.bienSo} onChange={this.searchStringChange} type="text" class="form-control" placeholder="Search for..." />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="submit">Tìm kiếm</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </Card>
                <Card style={{ marginBottom: "10px" }}>
                    <QuyTrinhView displayLink={false} height="65vh" quyTrinhs={this.state.pagination.content} style={{ marginBottom: "5px" }} loaiQuyTrinh=""></QuyTrinhView>
                    <table className="table" style={{ marginBottom: "0" }}>
                        <tfoot>
                            <tr>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    colSpan={7}
                                    count={this.state.pagination.totalElements}
                                    rowsPerPage={this.state.pagination.pageable.pageSize}
                                    page={this.state.pagination.pageable.pageNumber}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}

                                //ActionsComponent={TablePaginationActions}
                                />
                            </tr>
                        </tfoot>
                    </table>

                </Card>

            </div>
        )
    }
}
