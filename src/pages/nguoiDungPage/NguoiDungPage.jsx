import React, { Component } from 'react'
import { Grid, Button, TableRow, TableCell, TableHead, TableBody, Card, Table, TableFooter, TablePagination, Checkbox, TextField, Select, MenuItem, InputLabel, FormControl, IconButton, Icon, } from '@material-ui/core';
import { host } from '../../socket';
import axios from 'axios';
export default class NguoiDungPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nguoiDung: {
                idVaiTro: 2,
            },
            searchForm: {
                page: 0,
                pageSize: 10,
            },
            vaiTros: [],
            pagination: {
                content: [],
                pageable:
                {
                    pageSize: 10,
                    pageNumber: 0,
                },
                numberOfElements: 0,
            },
        }
        this.layDsNguoiDung = this.layDsNguoiDung.bind(this);
        this.layDsNguoiDung();
        this.getVaiTros = this.getVaiTros.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.getVaiTros();
        this.luu = this.luu.bind(this);
        this.khoa = this.khoa.bind(this);
        this.moKhoa = this.moKhoa.bind(this);
        this.xoa = this.xoa.bind(this);
    }
    getVaiTros() {
        var url = host + "/vai-tro/lay-ds";
        axios.get(url,
            {
                params: {
                    page: 0,
                    pageSize: 1000,
                }
            })
            .then((res) => {
                this.setState({
                    vaiTros: res.data,
                })
            });
    }
    layDsNguoiDung() {
        var url = host + "/nguoi-dung/lay-ds";
        axios.get(url,
            this.state.searchForm)
            .then((res) => {
                this.setState({
                    pagination: res.data
                })
            });
    }
    handleChangePage(event, newpage) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                page: newpage,
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageNumber: newpage,
                }
            },
        }), () => this.layDsNguoiDung());
    }
    handleChangeRowsPerPage(event) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                pageSize: event.target.value
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageSize: event.target.value,
                }
            },
        }), () => this.layDsNguoiDung());
        //this.getXes(this.state.pagination.pageable.pageNumber, event.target.value);

    }
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        var nguoiDung = this.state.nguoiDung;
        nguoiDung[name] = value;
        this.setState({
            nguoiDung: nguoiDung
        });
    }
    luu() {
        if (window.confirm("Bạn muốn thêm mới/cập nhật người dùng?")) {
            var nguoiDung = this.state.nguoiDung;
            var url = host + "/nguoi-dung/luu";
            axios.post(url, nguoiDung)
                .then(res => {
                    alert(res.data.resMessage);
                    this.layDsNguoiDung(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }
    }
    khoa(nguoiDung) {
        if (window.confirm("Bạn muốn khóa người dùng?")) {
            var url = host + "/nguoi-dung/khoa";
            axios.post(url, nguoiDung)
                .then(res => {
                    alert(res.data.resMessage);
                    this.layDsNguoiDung(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }
    }
    resetPwd(nguoiDung) {
        if (window.confirm("Bạn muốn đặt mật khẩu người dùng về mặc định?")) {
            var url = host + "/nguoi-dung/reset-pwd";
            axios.post(url, nguoiDung)
                .then(res => {
                    alert(res.data.resMessage);
                    this.layDsNguoiDung(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }
    }
    moKhoa(nguoiDung) {
        if (window.confirm("Bạn muốn mở khóa người dùng?")) {
            var url = host + "/nguoi-dung/mo-khoa";
            axios.post(url, nguoiDung)
                .then(res => {
                    alert(res.data.resMessage);
                    this.layDsNguoiDung(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }
    }
    xoa(nguoiDung) {
        if (window.confirm("Bạn muốn xóa người dùng?")) {
            var url = host + "/nguoi-dung/xoa";
            axios.post(url, nguoiDung)
                .then(res => {
                    alert(res.data.resMessage);
                    this.layDsNguoiDung(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }
    }
    render() {
        return (
            <div style={{ padding: "10px" }}>
                <div class="col-md-8">
                    <Card>
                        <table className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Họ tên</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Vai trò</th>
                                    <th style={{ textAlign: "center" }}>Khóa</th>
                                    <th style={{ textAlign: "center" }}>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.pagination.content.map((nguoiDung => {
                                    return (
                                        <tr>
                                            <td>
                                                {nguoiDung.hoTen}
                                            </td>
                                            <td>
                                                {nguoiDung.tenNguoiDung}
                                            </td>
                                            <td>
                                                {nguoiDung.tenVaiTro}
                                            </td>

                                            <td style={{ textAlign: "center", verticalAlign: "bottom" }}>
                                                <Icon>{nguoiDung.khoa ? "lock" : "lock_open"}</Icon>
                                            </td>
                                            <td style={{ width: "295px", }}>
                                                <Button title="Cập nhật thông tin" onClick={() => { this.setState({ nguoiDung: nguoiDung }) }} style={{ marginRight: "5px" }} variant="outlined" color="primary"><Icon>edit</Icon></Button>
                                                <Button title="Reset mật khẩu" onClick={() => { this.resetPwd(nguoiDung) }} style={{ marginRight: "5px" }} variant="outlined" color="primary"><Icon>restore</Icon></Button>
                                                <Button title="Khóa/mở khóa" onClick={() => {
                                                    if (nguoiDung.khoa) {
                                                        this.moKhoa(nguoiDung);
                                                    }
                                                    else
                                                        this.khoa(nguoiDung);
                                                }} style={{ marginRight: "5px" }} variant="outlined" color="default"><Icon>{nguoiDung.khoa ? "lock_open" : "lock"}</Icon></Button>
                                                <Button title="Xóa người dùng" onClick={() => {
                                                    this.xoa(nguoiDung);
                                                }} style={{ marginRight: "5px" }} variant="outlined" color="secondary"><Icon>delete</Icon></Button>
                                            </td>
                                        </tr>);
                                }))}
                            </tbody>
                            <tfoot>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    colSpan={5}
                                    count={this.state.pagination.totalElements}
                                    rowsPerPage={this.state.pagination.pageable.pageSize}
                                    page={this.state.pagination.pageable.pageNumber}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}

                                //ActionsComponent={TablePaginationActions}
                                />
                            </tfoot>
                        </table>
                    </Card>

                </div>
                <div className="col-md-4">
                    <Card style={{ marginBottom: "10px" }}>
                        <form>
                            <Grid
                                container
                                direction="row">
                                <Grid item md={12} style={{ padding: "15px" }}>
                                    <h4 style={{ letterSpacing: "1px" }}>Thêm người dùng mới</h4>
                                    <TextField name="hoTen" value={this.state.nguoiDung.hoTen} onChange={this.handleInputChange}
                                        style={{ width: "100%" }}
                                        id="hoTen"
                                        label="Họ tên"
                                        //className={classes.textField}
                                        //value={values.name}
                                        //onChange={handleChange('name')}
                                        margin="normal">
                                    </TextField>
                                    <TextField name="tenNguoiDung" value={this.state.nguoiDung.tenNguoiDung} onChange={this.handleInputChange}
                                        style={{ width: "100%" }}
                                        id="tenNguoiDung"
                                        label="Tên người dùng"
                                        //className={classes.textField}
                                        //value={values.name}
                                        //onChange={handleChange('name')}
                                        margin="normal">
                                    </TextField>
                                    <TextField
                                        style={{ width: "100%" }}
                                        id="idVaiTro"
                                        select
                                        label="Vai trò"
                                        name="idVaiTro"
                                        value={this.state.nguoiDung.idVaiTro}
                                        onChange={this.handleInputChange}
                                        margin="normal">
                                        {this.state.vaiTros.map((vaiTro) => {
                                            return <MenuItem value={vaiTro.idVaiTro}>{vaiTro.tenVaiTro}</MenuItem>
                                        })}
                                    </TextField>
                                </Grid>
                                <Grid item md={12} style={{ padding: "15px", textAlign: "center" }}>
                                    <Button style={{ marginRight: "15px" }} variant="contained" color="primary" onClick={this.luu}>Lưu</Button>
                                    <Button variant="contained" color="default" onClick={() => { this.setState({ nguoiDung: { hoTen: "", tenNguoiDung: "", idVaiTro: 2, idNguoiDung: null }, }) }}>Reset</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Card>

                </div>

            </div>
        )
    }
}
