import React, { Component } from 'react'
import { Card, TextField, Select, MenuItem, Checkbox, FormControlLabel, Button } from '@material-ui/core';
import { host } from '../../socket';
import axios from 'axios';
import PDFObject from 'pdfobject';
import MomentUtils from "@date-io/moment"; // choose your lib
import {
    KeyboardDateTimePicker,
    MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import "moment/locale/vi";
export default class BaoCaoPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchForm: {
                tuNgay: new Date((new Date()).setDate(new Date().getDate() - 30)),
                denNgay: new Date(),
                idDonVi: 0,
                loaiQuyTrinh: 1,
                tongHop: false,
            },
            donVis: [],
            pdfUrl: "",
            nguoiDungHienHanh: null,
        }
        this.getDonVis = this.getDonVis.bind(this);
        this.inputChange = this.inputChange.bind(this);
        this.xuatBaoCao = this.xuatBaoCao.bind(this);
        this.getDonVis();
        this.getNguoiDungHienHanh();
    }

    getNguoiDungHienHanh() {
        axios.get(host + "/authen/lay-nguoi-dung-hien-hanh")
            .then(res => {
                this.setState({
                    nguoiDungHienHanh: res.data
                })
            });
    }
    getDonVis = function () {
        var url = host + "/bao-cao/get-list";
        axios.get(url)
            .then(res => {
                this.setState({
                    donVis: res.data,
                });
            });
    };
    inputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                [name]: value,
            },
        }));
    }
    yyyymmdd(x) {
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        var h = x.getHours().toString();
        var mm = x.getMinutes().toString();
        var ss = x.getSeconds().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        (h.length == 1) && (h = '0' + h);
        (mm.length == 1) && (mm = '0' + mm);
        (ss.length == 1) && (ss = '0' + ss);
        var yyyymmdd = y + "-" + m + "-" + d + "T" + h + ":" + mm + ":" + ss;
        return yyyymmdd;
    }
    xuatBaoCao(e) {
        e.preventDefault();
        axios.post(host + "/bao-cao/xem-bao-cao", this.state.searchForm).then((res) => {
            this.setState(prevState => {
                return {
                    pdfUrl: host + "/" + res.data,
                };
            }, () => {
                PDFObject.embed(this.state.pdfUrl, `#pdf-container`);
            });
        });
        return false;
    }
    getDsLoaiQuyTrinhView() {
        if (this.state.nguoiDungHienHanh == null)
            return "";
        if (this.state.nguoiDungHienHanh.idVaiTro == 3 || this.state.nguoiDungHienHanh.idVaiTro == 4)
            return <TextField
                select
                name="loaiQuyTrinh"
                label="Loại quy trình"
                style={{ width: "100%" }}
                onChange={this.inputChange}
                value={this.state.searchForm.loaiQuyTrinh} >
                <MenuItem value="1">Nhập rác</MenuItem>
                <MenuItem value="2">Chôn rác</MenuItem>
            </TextField>;
        return <TextField
            select
            name="loaiQuyTrinh"
            label="Loại quy trình"
            style={{ width: "100%" }}
            onChange={this.inputChange}
            value={this.state.searchForm.loaiQuyTrinh} >
            <MenuItem value="0">Tất cả</MenuItem>
            <MenuItem value="1">Nhập rác</MenuItem>
            <MenuItem value="2">Chôn rác</MenuItem>
            <MenuItem value="3">Khác</MenuItem>
        </TextField>;
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <Card style={{ padding: "15px 5px" }}>
                            <MuiPickersUtilsProvider utils={MomentUtils} locale="vi">
                                <form onSubmit={this.xuatBaoCao} className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <KeyboardDateTimePicker
                                                style={{ fontSize: "14px" }}
                                                autoOk
                                                variant="inline"
                                                invalidDateMessage="Ngày sai định dạng!"
                                                ampm={false}
                                                onError={console.log}
                                                format="DD/MM/YYYY HH:mm:ss" type="datetime" name="tuNgay"
                                                label="Từ ngày" value={this.state.searchForm.tuNgay} onChange={(date) => {
                                                    if (date._isValid) {
                                                        this.setState(prevState => ({
                                                            searchForm: {
                                                                ...prevState.searchForm,
                                                                tuNgay: date,
                                                            },
                                                        }));
                                                    }
                                                }} />
                                        </div>
                                        <div className="col-md-2">
                                            <KeyboardDateTimePicker
                                                style={{ fontSize: "14px" }}
                                                autoOk
                                                variant="inline"
                                                invalidDateMessage="Ngày sai định dạng!"
                                                ampm={false}
                                                name="denNgay" format="DD/MM/YYYY HH:mm:ss"
                                                label="Đến ngày" value={this.state.searchForm.denNgay} onChange={(date) => {
                                                    if (date._isValid) {
                                                        this.setState(prevState => ({
                                                            searchForm: {
                                                                ...prevState.searchForm,
                                                                denNgay: date,
                                                            },
                                                        }));
                                                    }
                                                }} />
                                        </div>
                                        <div className="col-md-2">
                                            <TextField
                                                select
                                                label="Đơn vị"
                                                style={{ width: "100%",fontSize:"14px" }}
                                                id="donvi"
                                                name="idDonVi"
                                                value={this.state.searchForm.idDonVi}
                                                onChange={this.inputChange}>
                                                <MenuItem value={0}>Tất cả</MenuItem>
                                                {this.state.donVis.map((donVi) => {
                                                    return (<MenuItem value={donVi.idDonVi}>{donVi.tenDonVi}</MenuItem>);
                                                })}

                                            </TextField>
                                        </div>
                                        <div className="col-md-2">
                                            {this.getDsLoaiQuyTrinhView()}
                                        </div>
                                        <div className="col-md-2"
                                            style={{ verticalAlign: "bottom", textAlign: "center" }}>
                                            <FormControlLabel
                                                style={{ verticalAlign: "bottom" }}
                                                control={
                                                    <Checkbox
                                                        color="primary"
                                                        onChange={this.inputChange}
                                                        name="tongHop"
                                                        value={this.state.searchForm.tongHop}>
                                                    </Checkbox>
                                                }
                                                label="Tổng hợp" />
                                        </div>
                                        <div className="col-md-2"
                                            style={{ textAlign: "center" }}>
                                            <Button
                                                variant="contained"
                                                type="submit"

                                                color="primary">
                                                Xuất báo cáo
                                        </Button>
                                        </div>
                                    </div>
                                </form>
                            </MuiPickersUtilsProvider>
                            <div style={{ width: "100%", height: "calc(100vh - 165px)" }} id="pdf-container">
                                {this.state.pdfUrl == "" ? <div style={{ textAlign: "center", verticalAlign: "middle", fontStyle: "italic", color: "rgba(0,0,0,0.7)", paddingTop: "280px" }}>Chưa có dữ liệu</div> : ""}
                            </div>

                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}
