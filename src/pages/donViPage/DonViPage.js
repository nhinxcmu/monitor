import React, { Component } from 'react'
import { Card, IconButton, Button, TablePagination } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { host } from '../../socket';
import axios from 'axios';
export default class DonViPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pagination: {
        content: [],
        pageable:
        {
          pageSize: 10,
          pageNumber: 0,
        },
        numberOfElements: 0,
      },
      donVi: {},
      searchForm: {
        searchString: "",
        page: 0,
        pageSize: 10,
      }
    };
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
    this.searchStringChange = this.searchStringChange.bind(this);
    this.inputChange = this.inputChange.bind(this);
    this.searchFormSubmit = this.searchFormSubmit.bind(this);
    this.submit = this.submit.bind(this);
    this.xoa = this.xoa.bind(this);
    this.layDs = this.layDs.bind(this);
    this.layDs();
  }
  layDs() {
    axios.post(host + "/don-vi/lay-ds", this.state.searchForm).then((res) => {

      this.setState({
        pagination: res.data
      })
    });
  }
  searchStringChange(event) {
    const target = event.target;
    const value = target.value;
    this.setState(prevState => ({
      searchForm: {
        ...prevState.searchForm,
        searchString: value,
      },
    }));
  }
  handleChangePage(event, newpage) {
    this.setState(prevState => ({
      searchForm: {
        ...prevState.searchForm,
        page: newpage,
      },
      pagination: {
        ...prevState.pagination,
        pageable:
        {
          ...prevState.pagination.pageable,
          pageNumber: newpage,
        }
      },
    }), () => this.layDs());
    //this.getXes(newpage, this.state.pagination.pageable.pageSize);
  }
  handleChangeRowsPerPage(event) {
    this.setState(prevState => ({
      searchForm: {
        ...prevState.searchForm,
        pageSize: event.target.value
      },
      pagination: {
        ...prevState.pagination,
        pageable:
        {
          ...prevState.pagination.pageable,
          pageSize: event.target.value,
        }
      },
    }), () => this.layDs());
    //this.getXes(this.state.pagination.pageable.pageNumber, event.target.value);

  }
  inputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(event);
    this.setState(prevState => ({
      donVi: {
        ...prevState.donVi,
        [name]: value,
      },
    }));
  }
  searchFormSubmit(e) {
    e.preventDefault();
    this.layDs();
    return false;
  }

  submit(e) {
    e.preventDefault();
    if (window.confirm("Bạn muốn lưu đơn vị này?"))
    axios.post(host + "/don-vi/luu", this.state.donVi).then((res) => {
      if(res.data.idDonVi!=null)
      {
        window.alert("Thành công!");
      }
      else
      {
        window.alert("Lỗi! Lưu thất bại!");
      }
      this.layDs();
    });
    return false;
  }
  xoa(donVi) {
    if (window.confirm("Bạn muốn xóa đơn vị này?"))
      axios.post(host + "/don-vi/xoa", donVi).then((res) => {
        window.alert(res.data.resMessage);
        this.layDs();
      });
  }
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-8">
            <Card style={{ padding: "10px" }}>
              <div class="row">
                <form onSubmit={this.searchFormSubmit}>
                  <div className="form-group col-md-4 pull-right">
                    <div className="input-group input-group-sm">
                      <input onChange={this.searchStringChange} className="form-control" value={this.state.searchForm.searchString} />
                      <span className=" input-group-btn">
                        <button type="submit" className="btn btn-primary btn-sm">
                          <i style={{ fontSize: "20px" }} class="material-icons">
                            search
                        </i>
                        </button></span>

                    </div>
                  </div>
                </form>
              </div>
              <table className="table table-bordered table-hover table-sm">
                <thead>
                  <tr>
                    <th style={{ textAlign: "center" }}>
                      Mã đơn vị
                    </th>
                    <th>
                      Tên đơn vị
                    </th>
                    <th>
                      Số điện thoại
                    </th>
                    <th>
                      Địa chỉ
                    </th>
                    <th style={{ textAlign: "center" }}>
                      Chức năng
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.pagination.content.map((donVi) => {
                    return (
                      <tr key={donVi.idDonVi}>
                        <td style={{ textAlign: "center" }}>{donVi.idDonVi}</td>
                        <td>{donVi.tenDonVi}</td>
                        <td>{donVi.sdt}</td>
                        <td>{donVi.diaChi}</td>
                        <td style={{ textAlign: "center" }}>
                          <IconButton className="fa fa-edit"
                            onClick={() => {
                              this.setState({
                                donVi: donVi,
                              })
                            }}>
                            <Icon color="primary">edit</Icon>
                          </IconButton>
                          <IconButton className="fa fa-edit"
                            onClick={() => {
                              this.xoa(donVi);
                            }}>
                            <Icon color="error">delete</Icon>
                          </IconButton></td>
                      </tr>
                    );
                  })}
                </tbody>
                <tfoot>
                  <tr>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25]}
                      colSpan={7}
                      count={this.state.pagination.totalElements}
                      rowsPerPage={this.state.pagination.pageable.pageSize}
                      page={this.state.pagination.pageable.pageNumber}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}

                    //ActionsComponent={TablePaginationActions}
                    />
                  </tr>
                </tfoot>
              </table>
            </Card>
          </div>
          <div className="col-md-4">
            <Card>
              <div className="container-fluid">
                <form onSubmit={this.submit}>
                  <div className="row" style={{ marginTop: "5px" }}>
                    <div className="col-md-12">
                      <h4 style={{ letterSpacing: "1px" }}>Chi tiết đơn vị</h4>
                      <div className="form-group">
                        <label>Mã đơn vị</label>
                        <input name="idDonVi" onChange={this.inputChange} value={this.state.donVi != null ? this.state.donVi.idDonVi : ""} disabled className="form-control" />
                      </div>
                      <div className="form-group">
                        <label>Tên đơn vị</label>
                        <input name="tenDonVi" onChange={this.inputChange} value={this.state.donVi != null ? this.state.donVi.tenDonVi : ""} className="form-control" />
                      </div>
                      <div className="form-group">
                        <label>Số điện thoại</label>
                        <input name="sdt" onChange={this.inputChange} value={this.state.donVi != null ? this.state.donVi.sdt : ""} className="form-control" />
                      </div>
                      <div className="form-group">
                        <label>Địa chỉ</label>
                        <input name="diaChi" onChange={this.inputChange} value={this.state.donVi != null ? this.state.donVi.diaChi : ""} className="form-control" />
                      </div>
                    </div>
                  </div>

                  <div className="row" style={{ marginBottom: "15px" }}>
                    <div className="col-md-12" style={{ textAlign: "center" }}>
                      <Button type="submit" variant="contained" color="primary" style={{ marginLeft: "5px", marginRight: "5px" }}>
                        Lưu
                    </Button>
                      <Button
                        onClick={() => {
                          this.setState({
                            donVi: null
                          })
                        }}
                        variant="contained"
                        color="default"
                        style={{ marginLeft: "5px", marginRight: "5px" }}>
                        Hủy
                    </Button>
                    </div>
                  </div>
                </form>
              </div>
            </Card>
          </div>
        </div>
      </div>
    )
  }
}
