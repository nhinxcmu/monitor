import React, { Component } from 'react'
import { Grid, Button, TableRow, TableCell, TableHead, TableBody, Card, Table, TableFooter, TablePagination, Checkbox, TextField, Select, MenuItem, InputLabel, FormControl, } from '@material-ui/core';
import './danhMucXePage.css'

import { host, getDonVis } from '../../../socket';
import axios from 'axios';
import Xe from '../../../components/xe/Xe';

class DanhMucXePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchForm: {
                searchString: ""
            },
            searchString: "",
            show: false,
            pagination: {
                content: [],
                pageable:
                {
                    pageSize: 5,
                    pageNumber: 0,
                },
                numberOfElements: 0,
            },
            xe: {
                bienso: "",
                loaixe: 1,
                idDonVi: 1,
                trongLuongKhongTai: 1000,
            },
            donVis: []
        }
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.getXes = this.getXes.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.searchStringChange = this.searchStringChange.bind(this);
        this.luuXe = this.luuXe.bind(this);
        this.refresh = this.refresh.bind(this);
        this.getDonVis = getDonVis.bind(this);
        this.getDonVis();
        this.getXes();
    }
    showModal() {
        this.setState({
            show: true,
        })
    }
    hideModal() {
        this.setState({
            show: false
        });
    }
    getXes() {
        var url = host + "/xe/lay-ds";
        axios.get(url,
            {
                params: {
                    page: this.state.pagination.pageable.pageNumber,
                    pageSize: this.state.pagination.pageable.pageSize,
                    searchString: this.state.searchForm.searchString,
                }
            })
            .then((res) => {
                console.log(res.data);
                this.setState({
                    pagination: res.data
                })
            });
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        var xe = this.state.xe;
        xe[name] = value;
        this.setState({
            xe: xe
        });
    }
    searchStringChange(event) {
        const target = event.target;
        const value = target.value;
        console.log(value);
        this.setState((prevState) => {
            return {
                searchForm:
                {
                    ...prevState.searchForm,
                    searchString: value,
                },
            }
        }, () => this.getXes());


    }
    handleChangePage(event, newpage) {
        this.setState(prevState => ({
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageNumber: newpage,
                }
            },
        }), () => this.getXes());
        //this.getXes(newpage, this.state.pagination.pageable.pageSize);
    }
    handleChangeRowsPerPage(event) {
        this.setState(prevState => ({
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageSize: event.target.value,
                }
            },
        }), () => this.getXes());
        //this.getXes(this.state.pagination.pageable.pageNumber, event.target.value);

    }
    luuXe() {
        if (window.confirm("Bạn muốn thêm mới/cập nhật xe?")) {
            var xe = this.state.xe;
            var url = host + "/xe/luu";
            axios.post(url, xe)
                .then(res => {
                    alert(res.data.resMessage);
                    this.getXes(this.state.pagination.pageable.pageNumber, this.state.pagination.pageable.pageSize);
                });
        }

    }
    refresh() {
        this.getXes();
    }

    render() {
        return (
            <div classname="row">
                <div className="col-md-8">
                    <Card style={{ padding: "10px" }}>
                        <div class="row">
                            <form>
                                <div className="form-group col-md-4 pull-right">
                                    <div className="input-group input-group-sm">
                                        <input onChange={this.searchStringChange} className="form-control" value={this.state.searchForm.searchString} />
                                        <span className=" input-group-btn">
                                            <button type="submit" className="btn btn-primary btn-sm">
                                                <i style={{ fontSize: "20px" }} class="material-icons">
                                                    search
                                            </i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <Table className="table table-bordered table-striped custom-table">
                            <TableHead
                                classes={{
                                    root: 'custom-header'
                                }}>
                                <TableRow>
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>ID Xe</TableCell >
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>Biển số</TableCell>
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>Loại xe</TableCell>
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>Đơn vị</TableCell>
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>Trọng lượng không tải</TableCell>
                                    <TableCell style={{ textAlign: "center", fontSize: "1.2rem", fontWeight: "bold" }}>Thao tác</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.pagination.content.map(xe => {
                                    return (
                                        <Xe donVis={this.state.donVis} refresh={this.refresh} xe={xe}></Xe>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={6}
                                        count={this.state.pagination.totalElements}
                                        rowsPerPage={this.state.pagination.pageable.pageSize}
                                        page={this.state.pagination.pageable.pageNumber}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}

                                    //ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Card>
                </div>
                <div className="col-md-4">
                    <Card style={{ marginBottom: "10px" }}>
                        <form>
                            <Grid
                                container
                                direction="row">
                                <Grid item md={12} style={{ padding: "15px" }}>
                                <h4 style={{letterSpacing:"1px"}}>Thêm xe mới</h4>
                                    <TextField name="bienso" value={this.state.xe.bienso} onChange={this.handleInputChange}
                                        style={{ width: "100%" }}
                                        id="bienso"
                                        label="Biển số"
                                        //className={classes.textField}
                                        //value={values.name}
                                        //onChange={handleChange('name')}
                                        margin="normal">
                                    </TextField>
                                </Grid>
                                <Grid item md={12} style={{ padding: "15px" }}>
                                    <TextField
                                        style={{ width: "100%" }}
                                        id="loaiXe"
                                        select
                                        label="Loại xe"
                                        name="loaixe"
                                        value={this.state.xe.loaixe}
                                        onChange={this.handleInputChange}
                                        margin="normal">
                                        <MenuItem value="1">Xe nhập rác</MenuItem>
                                        <MenuItem value="3">Xe chôn rác</MenuItem>
                                        <MenuItem value="2">Xe cân ngoài</MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item md={12} style={{ padding: "15px" }}>
                                    <TextField
                                        style={{ width: "100%" }}
                                        id="donvi"
                                        select
                                        label="Đơn vị"
                                        name="idDonVi"
                                        value={this.state.xe.idDonVi}
                                        onChange={this.handleInputChange}
                                        margin="normal">
                                        {this.state.donVis.map((donVi) => {
                                            return (<MenuItem value={donVi.idDonVi}>{donVi.tenDonVi}</MenuItem>);
                                        })}

                                    </TextField>
                                </Grid>
                                <Grid item md={12} style={{ padding: "15px" }}>
                                    <TextField name="trongLuongKhongTai" value={this.state.xe.trongLuongKhongTai} onChange={this.handleInputChange}
                                        style={{ width: "100%" }}
                                        id="trongLuongKhongTai"
                                        label="Trọng lượng không tải"
                                        margin="normal">
                                    </TextField>
                                </Grid>
                                <Grid item md={12} style={{ padding: "15px", textAlign: "center" }}>
                                    <Button variant="raised" color="primary" onClick={this.luuXe}>Thêm mới</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Card>

                </div>
            </div>
        )
    }
}
export default DanhMucXePage;