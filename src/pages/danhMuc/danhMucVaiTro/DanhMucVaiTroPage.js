import React, { Component } from 'react'
import { Grid, Button, Card, Checkbox, CardHeader, TextField, Icon, IconButton, } from '@material-ui/core';

import { host } from '../../../socket';
import axios from 'axios';
import { CheckBox } from '@material-ui/icons';
import './danhMucVaiTro.css'
export default class DanhMucVaiTroPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //nguoiDungs: [],
            vaiTros: [],
            searchString: "",
            menus: [],
            vaiTro: {
                menus: [],
            },
            tenVaiTro: "",
        }
        this.getVaiTros = this.getVaiTros.bind(this);
        this.getVaiTros();
        this.getCayQuyen = this.getCayQuyen.bind(this);
        this.getCayQuyen();
        this.layDanhSachQuyen = this.layDanhSachQuyen.bind(this);
        this.luuVaiTro = this.luuVaiTro.bind(this);
        this.themMoi = this.themMoi.bind(this);
        this.inputChange = this.inputChange.bind(this);
    }
    getVaiTros() {
        var url = host + "/vai-tro/lay-ds";
        axios.get(url,
            {})
            .then((res) => {
                this.setState({
                    vaiTros: res.data,
                })
            });
    }
    getCayQuyen() {
        var url = host + "/vai-tro/du-lieu-cay";
        axios.get(url)
            .then((res) => {
                console.log(res.data);
                this.setState({
                    menus: res.data,
                })
            });
    }
    layDanhSachQuyen(vaiTro) {
        var url = host + "/vai-tro/lay-ct";
        axios.get(url,
            {
                params: {
                    id: vaiTro.idVaiTro
                }
            })
            .then((res) => {
                this.setState({
                    vaiTro: {
                        ...res.data,
                        menus: res.data.cayDtos,
                    },
                })
            });
    }
    onChecked = (menu, child) => {


        this.setState(state => {
            const menus = state.vaiTro.menus.map(_menu => {
                if (menu == _menu) {
                    return menu.children.map(_child => {
                        if (child == _child) {
                            if (child.selected) {


                            }
                            return child.selected = !child.selected;
                        }
                    })
                }
            });
            return menus;
        }, this.menuChange(menu, child));
    }
    menuChange = (menu, child) => {
        var vaiTro = this.state.vaiTro;
        if (vaiTro.menuDtos.filter(n => n.idMenu == child.key).length == 0) {
            vaiTro.menuDtos.push({ idMenu: child.key });
        }
        else {
            var _menu = vaiTro.menuDtos.filter(n => n.idMenu == child.key)[0];
            vaiTro.menuDtos.splice(vaiTro.menuDtos.indexOf(_menu), 1);
        }

        this.setState({
            vaiTro: vaiTro,
        }, () => {
            console.log(this.state.vaiTro);
        });
    }
    luuVaiTro() {
        if (window.confirm("Bạn muốn lưu thay đổi?"))
            axios.post(host + "/vai-tro/luu", this.state.vaiTro).then((res) => {
                window.alert(res.data.resMessage);
            });
    }
    themMoi() {
        if (window.confirm("Bạn muốn thêm mới vai trò?"))
            axios.post(host + "/vai-tro/luu", { idVaiTro: null, tenVaiTro: this.state.tenVaiTro }).then((res) => {
                window.alert(res.data.resMessage);
                this.getVaiTros();
                this.getCayQuyen();
                this.setState({ tenVaiTro: "" });
            });
    }
    xoaVaiTro(vaiTro) {
        if (window.confirm("Bạn muốn xóa vai trò?"))
            axios.post(host + "/vai-tro/xoa", vaiTro).then((res) => {
                window.alert(res.data.resMessage);
                this.getVaiTros();
                this.getCayQuyen();
                this.setState({ tenVaiTro: "" });
            });
    }
    inputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            tenVaiTro: value,
        }));
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-2"></div>
                    <div className="col-md-4">
                        <Card>
                            <span className="vai-tro-title">Vai trò</span>
                            {this.state.vaiTros.map(vaiTro => {
                                return (
                                    <Grid  container style={{
                                        padding: "10px",
                                        borderBottom: "1px solid rgba(0,0,0,0.1)"
                                    }}>
                                        <Grid className={(vaiTro.idVaiTro == this.state.vaiTro.idVaiTro ? "selected" : "") + " vai-tro"} item md={11}>
                                            <div  onClick={() => {
                                                this.layDanhSachQuyen(vaiTro);
                                            }}>
                                                {vaiTro.tenVaiTro}

                                            </div>
                                        </Grid>
                                        <Grid style={{textAlign:"center" }} item md={1}>
                                            <IconButton style={{marginTop:"5px", }} onClick={() => { this.xoaVaiTro(vaiTro); }} size="small" ><Icon style={{ color: "red" }}>delete</Icon></IconButton>
                                        </Grid>
                                        {/* <Grid item md={1} direction="row">
                                    <CheckBox values={true}></CheckBox>
                                    </Grid> */}
                                    </Grid>
                                );
                            })}
                            <Grid container style={{
                                padding: "10px",
                                borderBottom: "1px solid rgba(0,0,0,0.1)"
                            }}>
                                <Grid item md={8}>
                                    <TextField
                                        label="Tên vai trò"
                                        style={{ width: "100%", marginBottom: "10px" }}
                                        id="tenVaiTro"
                                        name="tenVaiTro"
                                        value={this.state.tenVaiTro}
                                        onChange={this.inputChange}>
                                    </TextField>
                                </Grid>
                                <Grid item md={4}>
                                    <Button style={{ marginTop: "16px", float: "right" }} onClick={this.themMoi} variant="contained" color="primary"><Icon>add_box</Icon><span>Thêm mới</span></Button>
                                </Grid>
                                {/* <Grid item md={1} direction="row">
                                    <CheckBox values={true}></CheckBox>
                                    </Grid> */}
                            </Grid>
                        </Card>
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-4">
                        <Card>
                            <span className="vai-tro-title">Quyền</span>
                            {this.state.vaiTro.menus.length == 0 ?
                                <div style={{ textAlign: "center" }}>
                                    <span style={{ fontStyle: "italic", color: "rgba(0,0,0,0.5)" }}>Chọn 1 mục bên bảng Vai trò</span>
                                </div>
                                :
                                (this.state.vaiTro.menus.map((menu) => {
                                    return <div className="menu">
                                        <span className="menu-title">{menu.title}</span>
                                        {menu.children.map((child) => {
                                            return <div className="child-menu" onClick={() => { this.onChecked(menu, child); }}>
                                                <Checkbox color="primary" checked={child.selected} /><span className="child-title">{child.title}</span>
                                            </div>;
                                        })}

                                    </div>;
                                }))}
                            {this.state.vaiTro.menus.length == 0 ? "" : <div style={{ textAlign: "center" }}>
                                <Button style={{ margin: "5px 0" }} variant="contained" color="primary" onClick={this.luuVaiTro}>Cập nhật</Button>
                            </div>}
                        </Card>
                    </div>
                    <div className="col-md-2"></div>
                </div>
            </div>
        )
    }
}
