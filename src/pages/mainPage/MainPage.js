import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap';
import DataView from './../../components/dataView/DataView';
import QuyTrinhView from './../../components/quyTrinhView/QuyTrinhView';
import DanhSachLoiView from './../../components/danhSachLoiView/DanhSachLoiView';
import DenBao from './../../components/denBao/DenBao';
import { CardContent, Card } from '@material-ui/core';
import posed from 'react-pose';
import { stompClient, host } from './../../socket';
import axios from 'axios';
const ListContainer = posed.div({
    enter: { staggerChildren: 50 },
    exit: { staggerChildren: 20, staggerDirection: -1 }
});

const Item = posed.div({
    enter: { y: 0, opacity: 1 },
    exit: { y: 50, opacity: 0 }
});
export default class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            den: 1,
            data: this.dataMockup(),
            quyTrinhs: [[], [], []],
            tops: [10, 10, 10],
            nguoiDungHienHanh: null,
        };
        this.ketNoi = this.ketNoi.bind(this);
        this.getQuyTrinhs = this.getQuyTrinhs.bind(this);
        this.onBottomScroll = this.onBottomScroll.bind(this);
        this.getDsLoiView = this.getDsLoiView.bind(this);
        this.ketNoi();
        this.getQuyTrinhs(1);
        this.getQuyTrinhs(2);
        this.getNguoiDungHienHanh();
    }
    getNguoiDungHienHanh() {
        axios.get(host + "/authen/lay-nguoi-dung-hien-hanh")
            .then(res => {
                this.setState({
                    nguoiDungHienHanh: res.data
                })
            });
    }
    getQuyTrinhs(loaiQuyTrinhID) {
        var tops = this.state.tops;
        var url = host + "/quyTrinh/getTop5QuyTrinh?loaiQuyTrinh=" + loaiQuyTrinhID + "&top=" + tops[loaiQuyTrinhID];
        axios.get(url)
            .then((res) => {
                var quyTrinhs = this.state.quyTrinhs;
                quyTrinhs[loaiQuyTrinhID] = res.data;
                this.setState({
                    quyTrinhs: quyTrinhs,
                });
            });
    }
    onBottomScroll(loaiQuyTrinhID) {
        var tops = this.state.tops;
        tops[loaiQuyTrinhID] = tops[loaiQuyTrinhID] + 10;
        this.setState({ tops: tops }, () =>
            this.getQuyTrinhs(loaiQuyTrinhID)
        );
    }
    dataMockup() {
        //Mockup Data
        return {
            biensotruoc1: "",
            biensotruoc2: "",
            biensosau1: "",
            biensosau2: "",
            cannang: "",
            hinhtruoc1: "",
            hinhtruoc2: "",
            hinhsau1: "",
            hinhsau2: "",
            hinhtrai: "",
            hinhphai: "",
        };
    }

    ketNoi() {
        stompClient.subscribe('/tinhieuden', (data) => {
            this.setState({
                den: JSON.parse(data.body),
            });
        });
        stompClient.subscribe('/dataview', (data) => {
            this.setState({
                data: JSON.parse(data.body),
            });
        });
        stompClient.subscribe('/getQuyTrinh', (data) => {
            this.getQuyTrinhs(1);
            this.getQuyTrinhs(2);
        });
    }
    getDsLoiView() {
        if (this.state.nguoiDungHienHanh == null)
            return "";
        if (this.state.nguoiDungHienHanh.idVaiTro != 1)
            return "";
        return <Item>
            <DanhSachLoiView style={{ marginBottom: "5px" }}></DanhSachLoiView>
        </Item>;
    }
    render() {
        if (stompClient.connected)
            return (
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card>
                                <CardContent>

                                    <DataView data={this.state.data}
                                        denBao={
                                            <DenBao status={this.state.den}></DenBao>}
                                    ></DataView>
                                </CardContent>
                            </Card>
                        </Col>
                        <Col md={4}>
                            <ListContainer>
                                {this.getDsLoiView()}
                                <Item>
                                    <QuyTrinhView loaiQuyTrinhID={1} onBottomScroll={this.onBottomScroll} displayLink={true} height="25vh" quyTrinhs={this.state.quyTrinhs[1]} style={{ marginBottom: "5px" }} loaiQuyTrinhID={1} loaiQuyTrinh="Nhập rác"></QuyTrinhView>
                                </Item>
                                <Item>
                                    <QuyTrinhView loaiQuyTrinhID={2} onBottomScroll={this.onBottomScroll} displayLink={true} height="25vh" quyTrinhs={this.state.quyTrinhs[2]} style={{ marginBottom: "0" }} loaiQuyTrinhID={2} loaiQuyTrinh="Chôn rác"></QuyTrinhView>
                                </Item>
                            </ListContainer>
                        </Col>
                    </Row>
                </Grid>
            )
        return (<div></div>);
    }
}
