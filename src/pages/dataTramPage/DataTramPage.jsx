import React, { Component } from 'react'
import { Table, TableRow, TableCell, TablePagination, Typography, TableHead, TableBody, Card,  TableFooter, } from '@material-ui/core';
import { host } from '../../socket';
import axios from 'axios';
import Loi from '../../components/loi/Loi';
import './dataTramPage.css'
export default class DataTramPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchForm: {
                bienSo: "",
                errorCode: -99,
                huong: -1,
                tuNgay: this.yyyymmdd((new Date((new Date()).setDate(new Date().getDate() - 30)))),
                denNgay: this.yyyymmdd(new Date()),
                idTram: -1,
                page: 0,
                pageSize: 10,
            },
            pagination: {
                content: [],
                pageable:
                {
                    pageSize: 10,
                    pageNumber: 0,
                },
                numberOfElements: 0,
            },
        }
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        //this.handleInputChange = this.handleInputChange.bind(this);
        this.searchStringChange = this.searchStringChange.bind(this);
        this.getDatatrams = this.getDatatrams.bind(this);
        this.submit = this.submit.bind(this);
        this.tuNgayChange = this.tuNgayChange.bind(this);
        this.denNgayChange = this.denNgayChange.bind(this);
        this.loiChange = this.loiChange.bind(this);
        this.huongChange = this.huongChange.bind(this);
        this.getDatatrams()
    }
    yyyymmdd(x) {
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        var h = x.getHours().toString();
        var mm = x.getMinutes().toString();
        var ss = x.getSeconds().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        (h.length == 1) && (h = '0' + h);
        (mm.length == 1) && (mm = '0' + mm);
        (ss.length == 1) && (ss = '0' + ss);
        var yyyymmdd = y + "-" + m + "-" + d + "T" + h + ":" + mm + ":" + ss;
        return yyyymmdd;
    }
    getDatatrams() {
        var url = host + "/datatram/lay-ds";
        axios.post(url,
            this.state.searchForm)
            .then((res) => {
                this.setState({
                    pagination: res.data
                })
            });
    }
    searchStringChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                bienSo: value,
            },
        }));
    }
    tuNgayChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                tuNgay: value,
            },
        }));
    }
    denNgayChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                denNgay: value,
            },
        }));
    }
    handleChangePage(event, newpage) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                page: newpage,
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageNumber: newpage,
                }
            },
        }), () => this.getDatatrams());
        //this.getXes(newpage, this.state.pagination.pageable.pageSize);
    }
    handleChangeRowsPerPage(event) {
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                pageSize: event.target.value
            },
            pagination: {
                ...prevState.pagination,
                pageable:
                {
                    ...prevState.pagination.pageable,
                    pageSize: event.target.value,
                }
            },
        }), () => this.getDatatrams());
        //this.getXes(this.state.pagination.pageable.pageNumber, event.target.value);

    }
    submit(e) {
        e.preventDefault();
        this.getDatatrams();
        return false;
    }
    huongChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                huong: value,
            },
        }));
    }
    loiChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                errorCode: value,
            },
        }));
    }
    tramChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                idTram: value,
            },
        }));
    }
    render() {
        return (
            <div style={{ padding: "10px" }}>
                <Card style={{ marginBottom: "10px", padding: "10px" }}>
                    <div className="container-fluid">
                        <form onSubmit={this.submit}>
                            <div className="row"><div class="col-md-3">
                                <label>Từ ngày</label>
                                <input onChange={this.tuNgayChange} value={this.state.searchForm.tuNgay} placeholder="Từ ngày" className="form-control" type="datetime-local" />
                            </div>
                                <div class="col-md-3">
                                    <label>Đến ngày</label>
                                    <input onChange={this.denNgayChange} value={this.state.searchForm.denNgay} placeholder="Đến ngày" className="form-control" type="datetime-local" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <label>Trạm</label>
                                    <select onChange={this.tramChange} class="form-control" value={this.state.searchForm.idTram} >
                                        <option value="-1">Tất cả</option>
                                        <option value="1">Cân</option>
                                        <option value="2">Bãi tập kết</option>
                                    </select>
                                </div>
                                <div className="col-md-3">
                                    <label>Hướng</label>
                                    <select onChange={this.huongChange} class="form-control" value={this.state.searchForm.huong} >
                                        <option value="-1">Tất cả</option>
                                        <option value="1">Xe ra</option>
                                        <option value="0">Xe vào</option>
                                    </select>
                                </div>
                                <div className="col-md-3">
                                    <label>Trạng thái</label>
                                    <select onChange={this.loiChange} class="form-control" value={this.state.searchForm.errorCode} >
                                        <option value="-99">Tất cả</option>
                                        <option value="-7">Cân nặng âm</option>
                                        <option value="-6">Lỗi quy trình</option>
                                        <option value="-5">Nhiều hơn 1 biển số có trong CSDL</option>
                                        <option value="-4">Thiếu cân nặng</option>
                                        <option value="-3">Xe đang trong quy trình khác</option>
                                        <option value="-2">Không có biển số xe gửi về</option>
                                        <option value="-1">Biển số xe không có trong CSDL</option>
                                        <option value="0">Thành công</option>
                                        <option value="1">Thiếu hình trước 1</option>
                                        <option value="2">Thiếu hình trước 2</option>
                                        <option value="3">Thiếu hình sau 1</option>
                                        <option value="4">Thiếu hình sau 2</option>
                                        <option value="5">Thiếu hình trái</option>
                                        <option value="6">Thiếu hình phải</option>
                                        <option value="7">Bỏ qua</option>
                                    </select>
                                </div>
                                <div className="col-md-3">
                                    <label>Biển số</label>
                                    <div class="input-group">
                                        <input value={this.state.searchForm.bienSo} onChange={this.searchStringChange} type="text" class="form-control" placeholder="Search for..." />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="submit">Tìm kiếm</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </Card>
                <Card style={{ marginBottom: "10px" }}>
                    <Table className="table table-hover table-bordered table-striped">
                        <TableHead>
                            <TableRow style={{ height: "20px" }}>
                                <TableCell style={{ width: "50px" }}>
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        Trạm
                            </Typography>
                                </TableCell>
                                <TableCell className="tg-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        Thời gian
                            </Typography>
                                </TableCell>
                                <TableCell className="loi-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        Lỗi
                            </Typography>
                                </TableCell>
                                <TableCell className="bs-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        BS 1
                            </Typography>
                                </TableCell>
                                <TableCell className="bs-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        BS 2
                            </Typography>
                                </TableCell>
                                <TableCell className="bs-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        BT 1
                            </Typography>
                                </TableCell>
                                <TableCell className="bs-cell">
                                    <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                        BT 2
                            </Typography>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.pagination.content.map(data => {
                                return (
                                    <Loi willDismiss={true} loi={data} key={"dataTram" + data.idTram}></Loi>
                                );
                            })}
                        </TableBody>
                        <TableFooter>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25]}
                                colSpan={7}
                                count={this.state.pagination.totalElements}
                                rowsPerPage={this.state.pagination.pageable.pageSize}
                                page={this.state.pagination.pageable.pageNumber}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}

                            //ActionsComponent={TablePaginationActions}
                            />
                        </TableFooter>
                    </Table>
                </Card>
            </div>
        )
    }
}
