import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import axios from 'axios';

//var host = "http://localhost:8889";
var host = "";
var socket = new SockJS(host + '/gsttslr-websocket');

var stompClient = Stomp.over(socket);
var errorCodes;
var url = host + "/errorcode/findAll"
axios.get(url).then(res => {
    errorCodes = res.data;
})
var getLois = function () {
    var url = host + "/datatram/getDataLoi";
    axios.get(url)
        .then(res => {

            this.setState({
                lois: res.data,
            });
        });
};
var getDonVis = function () {
    var url = host + "/don-vi/get-list";
    axios.get(url)
        .then(res => {
            this.setState({
                donVis: res.data,
            });
        });
};
var ping = function () {
    var url = host + "/ping";
    axios.get(url)
        .then(res => {
        });
}
var getDataTram = function (id, f) {
    var url = host + "/datatram/getDataTram?idDatatram=" + id;
    axios.get(url)
        .then(res => {
            f(res);
        });
}
var xemPhieuCan = function(idQuyTrinh,khachHang,matHang,ghiChu,f){
    var url = host + "/phieu-can/xem?idQuyTrinh=" + idQuyTrinh+"&khachHang="+khachHang+"&matHang="+matHang+"&ghiChu="+ghiChu;
    axios.get(url)
        .then(res => {
            f(res);
        });
}

export  { errorCodes, socket, host, stompClient, getLois, ping, getDataTram, getDonVis,xemPhieuCan};