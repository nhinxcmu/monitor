import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'
import CheckPoint from '../checkpoint/CheckPoint'
import CheckpointLine from '../checkpoint-line/CheckpointLine'
import './QuyTrinhDoRac.css'
import DataView from '../dataView/DataView'
import { TableRow, TableCell, Typography, Button } from '@material-ui/core';
import { KeyboardArrowDown } from '@material-ui/icons'
export default class QuyTrinhDoRac extends Component {
    constructor(props) {
        super(props);

        this.hideModal = this.hideModal.bind(this);
        this.showModal = this.showModal.bind(this);
        this.state = {
            show: false,
            data3Show:true,
            data1Show: true,
            data2Show: true,
        };
    }
    showModal() {
        this.setState({
            show: true
        });
    }
    hideModal() {
        this.setState({
            show: false
        });
    }

    roundButtonClick(dataID) {
        switch (dataID) {
            case 1:
                this.setState({
                    data1Show: !this.state.data1Show
                });
                break;
            case 2:
                this.setState({
                    data2Show: !this.state.data2Show
                });
                break;
            case 3:
                this.setState({
                    data3Show: !this.state.data3Show
                });
        }

    }
    render() {
        return (
            <TableRow className="quy-trinh">
                <TableCell component="th" onClick={this.showModal} style={{ width: "15%", padding: "10px", }}>
                    <Typography style={{ fontWeight: "bold" }} >
                    {this.props.quyTrinh.bienso != null ? this.props.quyTrinh.bienso : "88-H8 8888"}
                    </Typography>
                    <Typography >
                        {new Date(this.props.quyTrinh.data1.thoigian).toLocaleString('en-GB')}
                    </Typography>
                </TableCell>
                <TableCell onClick={this.showModal}>
                    <div className="truck-path"></div>
                    <div className="path">
                        <CheckPoint place="landfill" reached={true}></CheckPoint>
                        <CheckpointLine reached={this.props.quyTrinh.giaidoanhientai == 1 || this.props.quyTrinh.giaidoanhientai == 2 || this.props.quyTrinh.giaidoanhientai == 3}></CheckpointLine>
                        <CheckPoint value={this.props.quyTrinh.data2.cannang} place="scale" reached={this.props.quyTrinh.giaidoanhientai == 2 || this.props.quyTrinh.giaidoanhientai == 3}></CheckPoint>
                        <CheckpointLine value={this.props.quyTrinh.data2.cannang != null ? this.props.quyTrinh.data2.cannang - this.props.quyTrinh.data1.cannang : null} reached={this.props.quyTrinh.giaidoanhientai == 2 || this.props.quyTrinh.giaidoanhientai == 3}></CheckpointLine>
                        <CheckPoint place="landfill" reached={this.props.quyTrinh.giaidoanhientai == 2 || this.props.quyTrinh.giaidoanhientai == 3}></CheckPoint>
                        <CheckpointLine reached={this.props.quyTrinh.giaidoanhientai == 3}></CheckpointLine>
                        <CheckPoint value={this.props.quyTrinh.data3.cannang} place="scale" reached={this.props.quyTrinh.giaidoanhientai == 3}></CheckPoint>
                    </div>
                </TableCell>
                <TableCell component="th" onClick={this.showModal} style={{ width: "15%", padding: "10px", }}>
                    <Typography>
                        {this.props.quyTrinh.data2.thoigian!=null?new Date(this.props.quyTrinh.data2.thoigian).toLocaleString('en-GB'):""}
                    </Typography>
                </TableCell>
                <Modal bsSize="large" show={this.state.show} onHide={this.hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h3>Bãi tập kết, <b style={{color:"blue"}}>{new Date(this.props.quyTrinh.data3.thoigian).toLocaleString("en-GB")}</b></h3>

                        <div className="divider">
                            <div onClick={() => { this.roundButtonClick(3) }} className="round-button">
                                <KeyboardArrowDown style={{ transform: !this.state.data3Show ? "" : "rotate(180deg)" }}></KeyboardArrowDown>
                            </div>
                        </div>
                        <div className={"data-view-container"} style={{ height: !this.state.data3Show ? "0" : "auto" }}>
                            <DataView id={this.props.quyTrinh.data3.id} data={this.props.quyTrinh.data3}></DataView>
                        </div>
                        <h3> Cân lần 1, <b style={{color:"blue"}}>{new Date(this.props.quyTrinh.data1.thoigian).toLocaleString("en-GB")}</b> </h3>
                        <div className="divider">
                            <div onClick={() => { this.roundButtonClick(1) }} className="round-button">
                                <KeyboardArrowDown style={{ transform: !this.state.data1Show ? "" : "rotate(180deg)" }}></KeyboardArrowDown>
                            </div>
                        </div>
                        <div className={"data-view-container"} style={{ height: !this.state.data1Show ? "0" : "auto" }}>
                            {this.props.quyTrinh.data2.id != null ?
                                <DataView id={this.props.quyTrinh.data1.id} data={this.props.quyTrinh.data1}></DataView>
                                : <div style={{ width: "100%", height: "100px", textAlign: "center", padding: "10px;" }}>Chưa cân lần 1</div>}
                        </div>
                        <h3> Cân lần 2, <b style={{color:"blue"}}>{new Date(this.props.quyTrinh.data2.thoigian).toLocaleString("en-GB")} </b></h3>
                        <div className="divider">
                            <div onClick={() => { this.roundButtonClick(2) }} className="round-button">
                                <KeyboardArrowDown style={{ transform: !this.state.data2Show ? "" : "rotate(180deg)" }}></KeyboardArrowDown>
                            </div>
                        </div>
                        <div className={"data-view-container"} style={{ height: !this.state.data2Show ? "0" : "auto" }}>
                            {this.props.quyTrinh.data2.id != null ?
                                <DataView id={this.props.quyTrinh.data2.id} data={this.props.quyTrinh.data2}></DataView>
                                : <div style={{ width: "100%", height: "100px", textAlign: "center", padding: "10px;" }}>Chưa cân lần 2</div>}
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button  variant="outlined"  onClick={this.hideModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </TableRow>
        )
    }
}
