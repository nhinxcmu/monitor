import React, { Component } from 'react'
import { Card, CardHeader, CardContent, CardMedia, CardActions,Button, IconButton } from '@material-ui/core';
import { Popover,  OverlayTrigger,Modal } from 'react-bootstrap'
import './dataTramItem.css';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import { errorCode, tram } from '../../Utilities';
import DataView from '../dataView/DataView';

export default class DataTramItem extends Component {

    constructor(props) {
        super(props);
        this.popover = this.popover.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.state =
            {
                show: false,
            };

    }
    showModal() {
        this.setState({
            show: true,
        })
    }
    hideModal() {
        this.setState({
            show: false,
        })
    }
    popover() {
        return (
            <Popover id="popover-positioned-top" title="Chi tiết lỗi">
                <div><strong>Trạm: </strong>{tram(this.props.dataTram.idTram)}</div>
                <div><strong>Thời gian: </strong>{new Date(this.props.dataTram.thoigian).toLocaleString('en-GB')}</div>
                <div><strong>Lỗi: </strong>{errorCode(this.props.dataTram.errorcode)}</div>
            </Popover>
        );
    };
    render() {
        return (
            <div>
                <OverlayTrigger trigger="hover" placement="top" overlay={this.popover()}>
                    <Card className="datatram-item">
                        <CardMedia onClick={this.showModal} className="datatram-image" image={"data:image/jpg;base64," + this.props.dataTram.hinhtruoc1} />
                        <CardActions>
                            <IconButton style={{ padding: "5px" }} color="green" aria-label="Menu" onClick={(e) => {
                                this.props.addHandler(this.props.dataTram);
                            }}>
                                <AddIcon />
                            </IconButton>
                        </CardActions>
                    </Card>
                </OverlayTrigger>
                <Modal bsSize="large" show={this.state.show} onHide={this.hideModal}>
                    <Modal.Body>
                        <div className={"data-view-container"} >
                            <DataView id={this.props.dataTram.id} data={this.props.dataTram}></DataView>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outlined" onClick={this.hideModal}>Đóng</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
