import React, { Component } from 'react'
import { Modal, Grid, Row, Col, } from 'react-bootstrap'
import { TableRow, TableCell, Typography, Divider, Button } from '@material-ui/core';
import { getLois } from '../../socket';
import DataTramItem from '../dataTramItem/DataTramItem';
import DataTramSlot from '../dataTramSlot/DataTramSlot';
import { Scrollbars } from 'react-custom-scrollbars';
import './themQuyTrinh.css'
export default class ThemQuyTrinh extends Component {
    constructor(props) {
        super(props);
        this.getLois = getLois.bind(this);
        this.addDataTram = this.addDataTram.bind(this);
        this.removeDataTram = this.removeDataTram.bind(this);
        this.getLois();
        this.state = {
            quytrinh: null,
            lois: [],
            quyTrinh:
            {
                data1: null,
                data2: null,
                data3: null,
            }
        }
    }
    removeDataTram(dataTram, selected) {
        this.state.lois.push(dataTram);
        if (selected == 1)
            this.setState((prevState) => {
                prevState.quyTrinh.data1 = null;
                return {
                    quyTrinh: prevState.quyTrinh,
                }
            });
        if (selected == 2)
            this.setState((prevState) => {
                prevState.quyTrinh.data2 = null;
                return {
                    quyTrinh: prevState.quyTrinh,
                }
            });
        if (selected == 3)
            this.setState((prevState) => {
                prevState.quyTrinh.data3 = null;
                return {
                    quyTrinh: prevState.quyTrinh,
                }
            });
    }
    addDataTram(dataTram) {
        var index = this.state.lois.indexOf(dataTram);
        if (!this.state.quyTrinh.data3) {
            this.setState((prevState) => {
                prevState.lois.splice(index, 1);
                prevState.quyTrinh.data3 = dataTram;
                return {
                    quyTrinh: prevState.quyTrinh,
                    lois: prevState.lois,
                };
            })
        }
        else
            if (!this.state.quyTrinh.data1) {
                this.setState((prevState) => {
                    prevState.lois.splice(index, 1);
                    prevState.quyTrinh.data1 = dataTram;
                    return {
                        quyTrinh: prevState.quyTrinh,
                        lois: prevState.lois,
                    };
                })
            }
            else
                if (!this.state.quyTrinh.data2) {
                    this.setState((prevState) => {

                        prevState.lois.splice(index, 1);
                        prevState.quyTrinh.data2 = dataTram;
                        return {
                            quyTrinh: prevState.quyTrinh,
                            lois: prevState.lois,
                        };
                    })
                }

    }
    render() {
        return (
            <Modal bsSize="large" show={this.props.show} onHide={this.props.hideModal}>
                {/* <Modal.Header closeButton>
                    <Modal.Title>Thêm mới quy trình</Modal.Title>
                </Modal.Header> */}
                <Modal.Body>
                    <Grid>
                        <Row>
                            <Col md={4}>
                                <h4 className="slot-title">Bãi tập kết</h4>
                                <DataTramSlot removeDataTram={this.removeDataTram} dataTram={this.state.quyTrinh.data3} selected={3}></DataTramSlot>
                            </Col>
                            <Col md={4}>
                                <h4 className="slot-title">Cân lần 1</h4>
                                <DataTramSlot removeDataTram={this.removeDataTram} dataTram={this.state.quyTrinh.data1} selected={1}></DataTramSlot>
                            </Col>
                            <Col md={4}>
                                <h4 className="slot-title">Cân lần 2</h4>
                                <DataTramSlot removeDataTram={this.removeDataTram} dataTram={this.state.quyTrinh.data2} selected={2}></DataTramSlot>
                            </Col>
                        </Row>
                        <Divider style={{ marginBottom: "15px" }}></Divider>
                        <Row>
                            <Scrollbars style={{ height: "400px" }}>
                                {this.state.lois.map(loi =>
                                    <Col key={loi.idData} md={2}>
                                        <DataTramItem addHandler={this.addDataTram} key={loi.idData} dataTram={loi}></DataTramItem>
                                    </Col>
                                )}
                            </Scrollbars>
                        </Row>
                    </Grid>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outlined" onClick={this.props.hideModal}>Đóng</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
