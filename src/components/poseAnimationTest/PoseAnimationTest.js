import React, { Component } from 'react'
import posed from 'react-pose';
import { Button } from '@material-ui/core';
import anime from 'animejs';
import './poseAnimationTest.css'

const Box = posed.div(
    {
        smaller: {
            transform: "scale(0.9)",
        },
        normal: {
            transform: "scale(1)",
        },
        longer:
        {
            width: "200px"
        }
    }
);

class Fireflies extends Component {
    interval = 1+Math.random()*10;
    randWidthHeight = 3 + Math.random() * 10 + "px";
    constructor(props) {
        super(props);
        this.state = {
            top: Math.random() * 500,
            left: Math.random() * 1000,
            spread: Math.random() * 10 + "px",
        }
        setInterval(() => {
            var myArray = [1, -1];
            var randTop = myArray[Math.floor(Math.random() * myArray.length)];
            var randLeft = myArray[Math.floor(Math.random() * myArray.length)];
            var randSpread = myArray[Math.floor(Math.random() * myArray.length)];
            this.setState((prev) => {
                return {
                    top: prev.top + 25 * randTop>0&&prev.top + 25 * randTop<500?prev.top + 25 * randTop:prev.top + 25 *(-1),
                    left: prev.left + 25 * randLeft>0&& prev.left + 25 * randLeft<1000?prev.left + 25 * randLeft:prev.left + 25 *(-1),
                    spread: (prev.spread + (Math.random() * 1000  * randSpread)) + "px",
                }
            });
        }, this.interval * 1000);
    }
    render() {

        var style = {
            transition: "top "+this.interval+"s,left "+this.interval+"s linear",
            width: this.randWidthHeight,
            height: this.randWidthHeight,
            top: this.state.top,
            left: this.state.left,
            //boxShadow: "0 0 9px " + this.state.spread + " lime",
        };
        return (
            <span className="fireflies" style={style} ></span>
        )
    }
}
class Forest extends Component {
    render() {
        return (
            <div className="forest">
                {
                    this.props.children
                }
            </div>
        )
    }
}
export default class PoseAnimationTest extends Component {
    constructor(props) {
        super(props);
        this.state =
            {
                pose: "normal",

            }
        this.mouseDown = this.mouseDown.bind(this);
        this.mouseUp = this.mouseUp.bind(this);
        this.animeThis = this.animeThis.bind(this);
    }
    offset = 300;
    rand2 = 100;
    animeThis() {

        var myArray2 = [100, 200, 300, 352, 215, 122, 637, 117];
        myArray2.splice(this.rand2);
        this.rand2 = myArray2[Math.floor(Math.random() * myArray2.length)];

        anime({
            targets: '#anime',
            translateX: this.rand2 + Math.random() * 1000,
            rotate: '360deg',
            duration: 800,
            complete: function (data) {
                console.log(data);
            }
        });
    }
    sign() {
        anime({
            targets: '.sign',
            strokeDashoffset: [anime.setDashoffset, 0],
            easing: 'easeInOutSine',
            duration: 1500,
            delay: function (el, i) { return i * 250 },
            loop: true
        });
    }
    mouseDown() {
        this.setState({ pose: "smaller" });
    }
    mouseUp() {
        this.setState({ pose: "normal" });
    }

    render() {
        return <div>
            <Forest>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
                <Fireflies></Fireflies>
            </Forest>
            <Box onMouseEnter={this.animeThis} onMouseDown={this.mouseDown} onMouseUp={this.mouseUp} id="anime" style={{ width: "100px", height: "100px", background: "red" }} pose={this.state.pose}>
                Nhi
            </Box>
            <Button variant="outlined" onClick={() => {

                this.setState({
                    pose: "visible",
                });
            }}>Visible</Button>
            <Button variant="outlined" onClick={() => {

                this.setState({
                    pose: "hidden",
                });
            }}>Hidden</Button>
            <Button variant="outlined" onClick={this.animeThis}>Anime</Button>
        </div>
    }
}
