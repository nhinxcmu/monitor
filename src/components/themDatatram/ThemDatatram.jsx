import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'
import { Button, TextField, MenuItem } from '@material-ui/core';
import DataView from '../dataView/DataView';
import MomentUtils from "@date-io/moment"; // choose your lib
import axios from 'axios';
import { host } from '../../socket';
import {
    KeyboardDateTimePicker,
    MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import "moment/locale/vi";
const INIT_STATE_DATATRAM={ idData:-1,
    thoigian:new Date(),
    biensotruoc1:"",
    biensotruoc2:"",
    biensosau1:"",
    biensosau2:"",
    idTram:1,}
export default class ThemDatatram extends Component {
    constructor(props) {
        super(props);
        this.state = {
            datatram: INIT_STATE_DATATRAM
        }
        this.inputChange = this.inputChange.bind(this);
        this.luuDatatram = this.luuDatatram.bind(this);
    }


    inputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            datatram: {
                ...prevState.datatram,
                [name]: value,
            },
        }));
    }

    luuDatatram() {
        if (window.confirm("Bạn muốn thêm data trạm này?")) {
            axios.post(host + "/datatram/update", this.state.datatram).then((data) => {
                if (data.data == 200) {
                    alert("Thành công");
                }
                else {
                    alert("Thất bại");
                }
            });
        }
    }
    render() {
        return (
            <Modal bsSize="sm" show={this.props.show} onHide={this.props.hideModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Thêm mới Data Trạm</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={this.themDatatram} className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <TextField
                                    label="Cân nặng"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="cannang"
                                    name="cannang"
                                    value={this.state.datatram.cannang}
                                    onChange={this.inputChange}>
                                </TextField>
                                <TextField
                                    label="Biển số"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="biensotruoc1"
                                    name="biensotruoc1"
                                    value={this.state.datatram.biensotruoc1}
                                    onChange={this.inputChange}>
                                </TextField>
                                <TextField
                                    select
                                    label="Hướng"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="huong"
                                    name="huong"
                                    value={this.state.datatram.huong}
                                    onChange={this.inputChange}>
                                    <MenuItem value={0}>Vào</MenuItem>
                                    <MenuItem value={1}>Ra</MenuItem>
                                </TextField>
                                <MuiPickersUtilsProvider utils={MomentUtils} locale="vi">
                                    <KeyboardDateTimePicker
                                        style={{ width: "100%" }}
                                        autoOk
                                        variant="inline"
                                        invalidDateMessage="Ngày sai định dạng!"
                                        ampm={false}
                                        onError={console.log}
                                        format="DD/MM/YYYY HH:mm:ss" type="datetime" name="thoigian"
                                        label="Thời gian" value={this.state.datatram.thoigian} onChange={(date) => {
                                            if (date._isValid) {
                                                this.setState(prevState => ({
                                                    datatram: {
                                                        ...prevState.datatram,
                                                        thoigian: date,
                                                    },
                                                }));
                                            }
                                        }} />
                                </MuiPickersUtilsProvider>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button style={{ marginRight: "10px" }} variant="contained" color="primary" onClick={this.luuDatatram}>Lưu</Button>
                    <Button variant="contained" onClick={this.props.hideModal}>Đóng</Button>
                </Modal.Footer>
            </Modal >
        )
    }
}
