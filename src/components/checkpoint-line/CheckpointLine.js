import React, { Component } from 'react'
import './checkpointLine.css'

export default class CheckpointLine extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="checkpoint-line" style={ { backgroundColor: this.props.reached ?"limegreen":"" }}>
            </div>
        )
    }
}
