import React, { Component } from 'react'
import { errorCode, tram } from '../../Utilities'
import { Modal } from 'react-bootstrap'
import DataView from '../dataView/DataView';

export default class Loi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
        }
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }
    showModal() {
        this.setState({
            show: true,
        })
    }
    hideModal() {
        this.setState({
            show: false
        });
    }
    render() {
        return (
            <tr className="quy-trinh">
                <td onClick={this.showModal} style={{ width: "50px" }}>{tram(this.props.loi.idTram)}</td>
                <td className="tg-cell" onClick={this.showModal} >{new Date(this.props.loi.thoigian).toLocaleString('en-GB')}</td>
                <td className="loi-cell" onClick={this.showModal}>{errorCode(this.props.loi.errorcode)}</td>
                <td onClick={this.showModal} className="bs-cell">{this.props.loi.biensosau1}</td>
                <td onClick={this.showModal} className="bs-cell">{this.props.loi.biensosau2}</td>
                <td onClick={this.showModal} className="bs-cell">{this.props.loi.biensotruoc1}</td>
                <td onClick={this.showModal} className="bs-cell">{this.props.loi.biensotruoc2}</td>
                <Modal bsSize="large" show={this.state.show} onHide={this.hideModal}>
                    <Modal.Body>
                        <DataView callBack={this.props.willDismiss ? this.hideModal : () => { }} hideModal={this.hideModal} data={this.props.loi} edit={true}>

                        </DataView>
                    </Modal.Body>
                </Modal>
            </tr>
        )
    }
}
