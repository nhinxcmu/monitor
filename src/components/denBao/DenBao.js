import React, { Component } from 'react'
import './DenBao.css'
export default class DenBao extends Component {
    render() {
        return (
            <div style={{ width: "100%", textAlign: "center", }}>
                <h5>Đèn báo</h5>
                <div className="den-bao">
                    <div className="den" style={{ backgroundColor: (this.props.status == 1 ? "limegreen" : "") }}></div>
                    <div className="den" style={{ backgroundColor: (this.props.status == 2 ? "orange" : "") }}></div>
                    <div className="den" style={{ backgroundColor: (this.props.status == 3 ? "red" : "") }}></div>
                </div>

            </div>
        )
    }
}
