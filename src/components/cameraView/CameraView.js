import React from 'react';
import './cameraView.css'
import { Modal, } from 'react-bootstrap';
import {Card, CardContent,CardActions, Button } from '@material-ui/core';


class CameraView extends React.Component {

    constructor(props) {
        super(props);
        this.state =
            {
                imgModalShow: false,
            }
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }
    showModal() {
        this.setState({
            imgModalShow: true,
        });
    }
    hideModal() {
        this.setState({
            imgModalShow: false
        });
    }
    render() {
        return <div className="camera-view" style={this.props.style}>
            <img style={{ cursor: "pointer" }} onClick={this.showModal} height={this.props.height} width={this.props.width} src={this.props.image}></img>
            <div onClick={this.showModal} className="camera-name">
                {this.props.cameraName}
            </div>
            <Modal bsSize="large" onHide={this.hideModal} show={this.state.imgModalShow}>
                <Card>
                    <CardContent>
                        <h3>Camera {this.props.cameraName}</h3>
                        <img width="100%" src={this.props.image}></img>
                    </CardContent>
                    <CardActions style={{justifyContent:"flex-end"}}>
                        <Button variant="outlined" onClick={this.hideModal} >Đóng</Button>
                    </CardActions>
                </Card>
            </Modal>
        </div>;
    }
}
export default CameraView;
