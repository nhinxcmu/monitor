import React, { Component } from 'react'
import { TextField, MenuItem, Button, InputAdornment, Icon } from '@material-ui/core';
import { Modal } from 'react-bootstrap'
import { host } from '../../socket';
import axios from 'axios';
export default class DoiMatKhau extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                matKhauCu: "",
                matKhauMoi: "",
                xacNhanMatKhauMoi: "",
            }
        };
        this.inputChange = this.inputChange.bind(this);
        this.doiMatKhau = this.doiMatKhau.bind(this);
    }
    inputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            form: {
                ...prevState.form,
                [name]: value,
            },
        }));
    }
    doiMatKhau(e) {
        e.preventDefault();
        if (window.confirm("Bạn muốn đổi mật khẩu?")) {
            axios.post(host + "/authen/doi-mat-khau", this.state.form).then((data) => {
                if (data.data == 1) {
                    alert("Thành công");
                }
                else {
                    alert("Mật khẩu cũ không chính xác!");
                }
            });
        };
        return false;
    }
    render() {
        return (
            <Modal bsSize="sm" show={this.props.show} onHide={this.props.onHide}>
                <Modal.Header closeButton>
                    <Modal.Title>Đổi mật khẩu</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form className="container-fluid" id="form">
                        <div className="row">
                            <div className="col-md-12">
                                <TextField
                                    type="password"
                                    label="Mật khẩu cũ"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="matKhauCu"
                                    name="matKhauCu"
                                    value={this.state.form.matKhauCu}
                                    onChange={this.inputChange}>
                                </TextField>
                                <TextField
                                    type="password"
                                    label="Mật khẩu mới"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="matKhauMoi"
                                    name="matKhauMoi"
                                    value={this.state.form.matKhauMoi}
                                    onChange={this.inputChange}>
                                </TextField>
                                <TextField
                                
                                    error={this.state.form.matKhauMoi != this.state.form.xacNhanMatKhauMoi}
                                    type="password"
                                    label="Xác nhận mật khẩu mới"
                                    style={{ width: "100%", marginBottom: "10px" }}
                                    id="xacNhanMatKhauMoi"
                                    name="xacNhanMatKhauMoi"
                                    value={this.state.form.xacNhanMatKhauMoi}
                                    onChange={this.inputChange}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">{this.state.form.xacNhanMatKhauMoi == this.state.form.matKhauMoi ? <Icon style={{ color: "green", fontWeifht: "bold" }}>check</Icon> : <Icon style={{ color: "red", fontWeight: "bold" }}>close</Icon>}</InputAdornment>,
                                    }}>
                                </TextField>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="submit" form="form" disabled={this.state.form.matKhauMoi != this.state.form.xacNhanMatKhauMoi} style={{ marginRight: "10px" }} variant="contained" color="primary" onClick={this.doiMatKhau}>Lưu</Button>
                    <Button variant="contained" onClick={this.props.onHide}>Đóng</Button>
                </Modal.Footer>
            </Modal >
        )
    }
}
