import React, { Component } from 'react'
import { Modal, } from 'react-bootstrap'
import CheckPoint from '../checkpoint/CheckPoint'
import './QuyTrinhKhac.css'
import DataView from '../dataView/DataView'
import { TableRow, TableCell, Typography, Button, TextField } from '@material-ui/core';
import { KeyboardArrowDown } from '@material-ui/icons'
import { xemPhieuCan, host } from '../../socket';
export default class QuyTrinhKhac extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            data1Show: true,
            searchForm: {
                khachHang: "",
                matHang: "",
                ghiChu: ""
            }
        };
        this.hideModal = this.hideModal.bind(this);
        this.showModal = this.showModal.bind(this);
        this.roundButtonClick = this.roundButtonClick.bind(this);
        this.inputChange = this.inputChange.bind(this);
    }
    showModal() {
        this.setState({
            show: true
        });
    }
    hideModal() {
        this.setState({
            show: false
        });
    }
    roundButtonClick(dataID) {
        switch (dataID) {
            case 1:
                this.setState({
                    data1Show: !this.state.data1Show
                });
                break;
            case 2:
                this.setState({
                    data2Show: !this.state.data2Show
                });
                break;
            default: break;
        }
    }
    inputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            searchForm: {
                ...prevState.searchForm,
                [name]: value,
            },
        }));
    }
   
    render() {
        return (
            <TableRow onClick={this.getDataTram} className="quy-trinh" style={{ cursor: "pointer" }}>
                <TableCell component="th" onClick={this.showModal} style={{ width: "15%", padding: "10px", }}>
                    <Typography style={{ fontWeight: "bold" }} >
                        {this.props.quyTrinh.bienso}
                    </Typography>
                    <Typography>
                        {new Date(this.props.quyTrinh.data1.thoigian).toLocaleString('en-GB')}
                    </Typography>
                </TableCell>
                <TableCell style={{ padding: "0" }} onClick={this.showModal}>
                    <div className="path">
                        <CheckPoint value={this.props.quyTrinh.data1.cannang} place="scale" reached={true}></CheckPoint>
                    </div>
                </TableCell>
                <TableCell style={{ padding: "0" }} onClick={this.showModal}></TableCell>
                <Modal bsSize="large" show={this.state.show} onHide={this.hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.quyTrinh.BienSo}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            <div className="col-md-12">
                                <form className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <TextField
                                                style={{ width: "100%" }}
                                                type="text" name="khachHang"
                                                label="Khách hàng"
                                                value={this.state.searchForm.khachHang}
                                                onChange={this.inputChange}>
                                            </TextField>
                                        </div>
                                        <div className="col-md-3">
                                            <TextField
                                                style={{ width: "100%" }}
                                                type="text" name="matHang"
                                                label="Mặt hàng"
                                                value={this.state.searchForm.matHang}
                                                onChange={this.inputChange}>
                                            </TextField>
                                        </div>
                                        <div className="col-md-3">
                                            <TextField
                                                style={{ width: "100%" }}
                                                type="text" name="ghiChu"
                                                label="Ghi chú"
                                                value={this.state.searchForm.ghiChu}
                                                onChange={this.inputChange}>
                                            </TextField>
                                        </div>
                                        <div className="col-md-3"
                                            style={{ textAlign: "center" }}>
                                            <a className="btn btn-success" href={host + "/phieu-can/in?idQuyTrinh=" + this.props.quyTrinh.idQuytrinh+"&khachHang="+ this.state.searchForm.khachHang+"&matHang="+ this.state.searchForm.matHang+"&ghiChu="+ this.state.searchForm.ghiChu}>
                                                Xuất phiếu cân
                                        </a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                        <h3><b style={{ color: "blue" }}>{new Date(this.props.quyTrinh.data1.thoigian).toLocaleString("en-GB")}</b>  </h3>

                        <div className="divider">
                            <div onClick={() => { this.roundButtonClick(1) }} className="round-button">
                                <KeyboardArrowDown style={{ transform: !this.state.data1Show ? "" : "rotate(180deg)" }}></KeyboardArrowDown>
                            </div>
                        </div>
                        <div className={"data-view-container"} style={{ height: !this.state.data1Show ? "0" : "auto" }}>
                            <DataView id={this.props.quyTrinh.data1.idData} data={this.props.quyTrinh.data1}></DataView>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outlined" onClick={this.hideModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </TableRow>
        )
    }
}
