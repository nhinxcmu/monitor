import React, { Component } from 'react'
import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { Link, } from 'react-router-dom';
import axios from 'axios';
import { host } from '../../socket';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


export default class MainDrawer extends Component {
    constructor(props) {
        super()
        this.state = {
            menus: this.menuMockup(),
        }
        this.getMenu=this.getMenu.bind(this);
        this.getMenu();
    }
    menuMockup() {
        return [{"idMenu":1,"url":"#","title":"Danh mục","icon":"<i class=\"fa fa-indent\"></i>","orderMenu":100,"menuDtos":[{"idMenu":2,"url":"nguoi-dung","title":"Người dùng","icon":"<i class=\"fa fa-circle-o\"></i>","orderMenu":1,"menuDtos":[]},{"idMenu":3,"url":"vai-tro","title":"Vai trò","icon":"<i class=\"fa fa-circle-o\"></i>","orderMenu":2,"menuDtos":[]},{"idMenu":35,"url":"xe","title":"Xe","icon":"<i/>","orderMenu":3,"menuDtos":[]}]},{"idMenu":4,"url":"#","title":"Chức năng","icon":"<i class=\"fa fa-list\"></i>","orderMenu":101,"menuDtos":[{"idMenu":5,"url":"can","title":"Cân","icon":"<i class=\"fa fa-circle-o\"></i>","orderMenu":1,"menuDtos":[]},{"idMenu":6,"url":"giam-sat","title":"Giám sát","icon":"<i/>","orderMenu":2,"menuDtos":[]}]}];
    }
    getMenu() {
        axios.get(host + "/authen/getMenus").then((res) => {
            this.setState(
                {
                    menus: res.data,
                }
            )
        });
    }
    render() {
        const sideList = (
            <div>
                {this.state.menus.map((menu) => {
                    return (
                        <ExpansionPanel style={{ marginBottom: "0", marginTop: "0" }}>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography style={{fontSize:"1.2rem"}}>{menu.title}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails style={{ padding: 0 }}>
                                <List style={{ width: 250 }}>
                                    {menu.menuDtos.map(menuCon => {
                                        return (
                                            <Link
                                            style={{color:"black",textDecoration:"none!important" }}
                                                onClick={() => this.props.toggleDrawer(false)}
                                                onKeyDown={() => this.props.toggleDrawer(false)}
                                                to={menuCon.url}>
                                                <ListItem button key={menuCon.url}>
                                                    <ListItemIcon><Icon style={{fontSize:"1.5rem"}}>{menuCon.icon}</Icon></ListItemIcon>
                                                    <ListItemText style={{fontSize:"1.5rem"}} primary={menuCon.title} />
                                                </ListItem>
                                            </Link>
                                        );
                                    })}
                                </List>


                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    )
                })}
            </div>
        );

        return (
            <div>
                <Drawer open={this.props.open} onClose={() => { this.props.toggleDrawer(false) }}>
                    <div
                        tabIndex={0}
                        role="button">
                        {sideList}
                    </div>
                </Drawer>
            </div>
        )
    }
}
