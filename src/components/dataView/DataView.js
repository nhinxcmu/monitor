import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import CameraView from '../cameraView/CameraView';
import './dataView.css';
import truck from './truck.jpg';
import noimage from './noimage.png';
import { CardContent, Card } from '@material-ui/core'
import posed from 'react-pose';
import { Button } from '@material-ui/core';
import axios from 'axios';
import { host, getDataTram } from '../../socket';
const ListContainer = posed.div({
  enter: { staggerChildren: 50 },
  exit: { staggerChildren: 20, staggerDirection: -1 }
});

const Item = posed.div({
  enter: { y: 0, opacity: 1 },
  exit: { y: 50, opacity: 0 }
});
class DataView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      dieuChinhDisabled: false,
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.submit = this.submit.bind(this);
    this.boQua = this.boQua.bind(this);
    getDataTram(this.state.data.idData, (res) => {
      this.setState({
        data: res.data,
      });
    })
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    var data = this.state.data;
    data[name] = value;
    this.setState({
      data: data
    });
  }
  componentWillReceiveProps(nextProps) {
    if ((this.props.data.hinhtruoc1 == ""
      && this.props.data.hinhtruoc2 == ""
      && this.props.data.hinhsau1 == ""
      && this.props.data.hinhsau1 == ""
      && this.props.data.hinhtrai == ""
      && this.props.data.hinhphai == "") || nextProps.data.idData != this.props.data.idData)
      getDataTram(nextProps.data.idData, (res) => {
        this.setState({
          data: res.data,
        });
      })
  }
  submit(event) {
    this.setState({
      dieuChinhDisabled: true,
    }, () => {
      axios.post(host + "/datatram/update", this.state.data).then((res) => {
        if (res.data == 200) {
          alert("Thành công");
        }
        else {
          alert("Thất bại");
        }
        getDataTram(this.state.data.idData, (res) => {
          this.setState({
            data: res.data,
            dieuChinhDisabled: false,
          },this.props.callBack);
        });
      });
    });

    event.preventDefault();
  }

  boQua() {
    this.setState({
      dieuChinhDisabled: true,
    }, () => {
      var dataTram = this.state.data;
      axios.post(host + "/datatram/bo-qua", dataTram).then((data) => {
        if (data.data.idData != null) {
          alert("Thành công");
          this.setState({
            dieuChinhDisabled: false,
          },this.props.callBack);
        }
        else {
          alert("Thất bại");
        }
      });
    })

  }
  render() {
    return <Grid fluid={true} style={this.props.style}>
      <Row>
        <ListContainer>
          <Item><Col md={10}>
            <Row>
              <Col md={6} style={{ textAlign: "center" }}>
                <CameraView
                  width="100%"
                  image={this.state.data.hinhtruoc1 != "" && this.state.data.hinhtruoc1 != null ? "data:image/jpg;base64," + this.state.data.hinhtruoc1 : noimage}
                  cameraName="Trước 1">
                </CameraView>
              </Col>
              <Col md={6} style={{ textAlign: "center" }}>
                <CameraView
                  width="100%"
                  image={this.state.data.hinhtruoc2 != "" && this.state.data.hinhtruoc2 != null ? "data:image/jpg;base64," + this.state.data.hinhtruoc2 : noimage}
                  cameraName="Trước 2">
                </CameraView>
              </Col>
            </Row>
            <Row>
              <Col md={6} style={{ textAlign: "center" }}>

                <CameraView style={{ width: "100%" }}
                  width="100%"
                  image={this.state.data.hinhtrai != "" && this.state.data.hinhtrai != null ? "data:image/jpg;base64," + this.state.data.hinhtrai : noimage}
                  cameraName="Trái">
                </CameraView>

              </Col>
              <Col md={6} style={{ textAlign: "center" }}>

                <CameraView style={{ width: "100%" }}
                  width="100%"
                  image={this.state.data.hinhphai != "" && this.state.data.hinhphai != null ? "data:image/jpg;base64," + this.state.data.hinhphai : noimage}
                  cameraName="Phải">
                </CameraView>
              </Col>
            </Row>
            <Row>
              <Col md={6} style={{ textAlign: "center" }}>
                <CameraView
                  width="100%"
                  image={this.state.data.hinhsau1 != "" && this.state.data.hinhsau1 != null ? "data:image/jpg;base64," + this.state.data.hinhsau1 : noimage}
                  cameraName="Sau 1">
                </CameraView>
              </Col>
              <Col md={6} style={{ textAlign: "center" }}>
                <CameraView
                  width="100%"
                  image={this.state.data.hinhsau2 != "" && this.state.data.hinhsau2 != null ? "data:image/jpg;base64," + this.state.data.hinhsau2 : noimage}
                  cameraName="Sau 2">
                </CameraView>
              </Col>
            </Row>
          </Col>
          </Item>
        </ListContainer>
        <ListContainer>
          <form onSubmit={this.submit}>
            <Col md={2}>
              <Row>
                <Col>
                  <Item>
                    <Card style={{ marginBottom: "5px" }}>
                      <CardContent>
                        <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)", marginBottom: "8px" }}>
                          <span className="tittle">Thời gian</span>
                        </div>
                        <div style={{ textAlign: "center" }}>
                          {this.state.data.thoigian != null ? <label>{new Date(this.state.data.thoigian).toLocaleString("en-GB")}</label> : ""}
                        </div>
                      </CardContent>
                    </Card>
                  </Item>
                  <Item><Card style={{ marginBottom: "5px" }}>
                    {this.props.denBao != null ? this.props.denBao : ""}
                  </Card></Item>
                  <Item><Card style={{ marginBottom: "5px" }}>
                    <CardContent>
                      <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)" }}>
                        <span className="tittle">Cân nặng</span>
                      </div>
                      <div style={{ textAlign: "center" }}>
                        <span style={{ fontWeight: "bold", fontSize: "2.1em", wordBreak: "break-word" }}>{this.state.data.cannang}</span>
                      </div>
                    </CardContent>
                  </Card>
                  </Item>
                  <Item>
                    <Card style={{ marginBottom: "5px" }}>
                      <CardContent>
                        <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)", marginBottom: "8px" }}>
                          <span className="tittle">Biển Trước 1</span>
                        </div>
                        <div style={{ textAlign: "center" }}>
                          <input type="text" className="bienSo form-control" name="biensotruoc1" value={this.state.data.biensotruoc1} disabled={!this.props.edit} onChange={this.handleInputChange} />
                        </div>
                      </CardContent>
                    </Card>
                  </Item>
                  <Item>
                    <Card style={{ marginBottom: "5px" }}>
                      <CardContent>
                        <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)", marginBottom: "8px" }}>
                          <span className="tittle">Biển Trước 2</span>
                        </div>
                        <div style={{ textAlign: "center" }}>
                          <input type="text" className="bienSo form-control" name="biensotruoc2" value={this.state.data.biensotruoc2} disabled={!this.props.edit} onChange={this.handleInputChange} />
                        </div>
                      </CardContent>
                    </Card>
                  </Item>
                  <Item><Card style={{ marginBottom: "5px" }}>
                    <CardContent>
                      <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)", marginBottom: "8px" }}>
                        <span className="tittle">Biển Sau 1</span>
                      </div>
                      <div style={{ textAlign: "center" }}>
                        <input type="text" className="bienSo form-control" name="biensosau1" value={this.state.data.biensosau1} disabled={!this.props.edit} onChange={this.handleInputChange} />
                      </div>
                    </CardContent>
                  </Card>
                  </Item>
                  <Item>
                    <Card style={{ marginBottom: "5px" }}>
                      <CardContent>
                        <div style={{ textAlign: "center", borderBottom: "1px solid rgba(0,0,0,0.3)", marginBottom: "8px" }}>
                          <span className="tittle">Biển Sau 2</span>
                        </div>
                        <div style={{ textAlign: "center" }}>
                          <input type="text" className="bienSo form-control" name="biensosau2" value={this.state.data.biensosau2} disabled={!this.props.edit} onChange={this.handleInputChange} />
                        </div>
                      </CardContent>
                    </Card>
                  </Item>
                </Col>
              </Row>
              {this.props.edit ? <Row>
                <Button disabled={this.state.dieuChinhDisabled} style={{ margin: "0 5px" }} variant="contained" type="submit" color="primary">Điều chỉnh</Button>
                <Button disabled={this.state.dieuChinhDisabled} onClick={this.boQua} variant="contained" color="secondary">Bỏ qua</Button>
              </Row> : ""}
            </Col>
          </form>
        </ListContainer>
      </Row>
      <Row>
        {this.props.edit ? <Button style={{ float: "right" }} onClick={this.props.hideModal} variant="outlined">Đóng</Button> : ""}</Row>
    </Grid>;
  }
}
export default DataView;
