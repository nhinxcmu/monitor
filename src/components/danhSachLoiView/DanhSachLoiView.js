import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './danhSachLoi.css'
import Loi from '../loi/Loi'
import { Scrollbars } from 'react-custom-scrollbars';
import { stompClient, getLois } from '../../socket';
import { TableRow, TableCell, Typography, TableHead, TableBody, Card, IconButton, } from '@material-ui/core';
import ThemQuyTrinh from '../themQuyTrinh/ThemQuyTrinh';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import ThemDatatram from '../themDatatram/ThemDatatram';

export default class DanhSachLoiView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lois: [],
            modal: false,
        }
        this.getLois = getLois.bind(this);
        this.getLois();
        this.ketNoi = this.ketNoi.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.ketNoi();
    }
    ketNoi() {
        stompClient.subscribe("/getLoi", data => {
            console.log("socket get lois");
            this.getLois();
        });
    }
    showModal() {
        this.setState({
            modal: true
        });
    }
    hideModal() {
        this.setState({
            modal: false
        });
    }
    render() {
        var lois = [];
        var _lois = this.state.lois
        _lois.forEach(function (loi) {
            lois.push(
                <Loi willDismiss={false} key={"loi" + _lois.indexOf(loi)} loi={loi}></Loi>
            );
        });
        return <Card style={this.props.style}>
            <div style={{ textAlign: "center", padding: "10px" }}>
                <div>
                    <IconButton style={{ float: "left", padding: "0" }}  onClick={() => {
                        this.showModal();
                    }}>
                        <AddIcon />
                    </IconButton>
                </div>
                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                    Danh sách lỗi
                    {/* <IconButton style={{ float: "right", padding: "0" }} color="inherit" aria-label="Menu" onClick={() => {
                        this.showModal();
                    }}>
                        <AddIcon />
                    </IconButton> */}<a style={{ float: "right" }} href="/data-tram">Xem tất cả >></a>
                </Typography>


            </div>
            <Scrollbars style={{ height: "24vh" }}>
                <Table style={{ width: "610px" }} bordered condensed hover>
                    <TableHead>
                        <TableRow style={{ height: "20px" }}>
                            <TableCell style={{ width: "50px" }}>
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    Trạm
                            </Typography>
                            </TableCell>
                            <TableCell className="tg-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    Thời gian
                            </Typography>
                            </TableCell>
                            <TableCell className="loi-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    Lỗi
                            </Typography>
                            </TableCell>
                            <TableCell className="bs-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    BS 1
                            </Typography>
                            </TableCell>
                            <TableCell className="bs-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    BS 2
                            </Typography>
                            </TableCell>
                            <TableCell className="bs-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    BT 1
                            </Typography>
                            </TableCell>
                            <TableCell className="bs-cell">
                                <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                    BT 2
                            </Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {lois}
                    </TableBody>
                </Table>
            </Scrollbars>

            {/* <ThemQuyTrinh show={this.state.modal} hideModal={this.hideModal}></ThemQuyTrinh> */}
            <ThemDatatram show={this.state.modal} hideModal={this.hideModal}></ThemDatatram>
        </Card>;
    }
}
