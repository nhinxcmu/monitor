import React from 'react';
import { Table, TableHead, TableFooter, TableRow, TableCell, TableBody, Card, Typography, Link, TablePagination } from '@material-ui/core';
import QuyTrinhNhapRac from '../quyTrinhNhapRac/QuyTrinhNhapRac'
import QuyTrinhDoRac from '../quyTrinhDoRac/QuyTrinhDoRac'
import './quyTrinhView.css';
import axios from 'axios';
import { host, getDataTram } from '../../socket';
import { Scrollbars } from 'react-custom-scrollbars';
import QuyTrinhKhac from '../quyTrinhKhac/QuyTrinhKhac';
import CircularProgress from '@material-ui/core/CircularProgress';
class QuyTrinhView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            recordPerLoad: 5,
        }
        this.onScroll = this.onScroll.bind(this);
    }
    onScroll(values) {
        if (values.top >= 1) {
            this.props.onBottomScroll(this.props.loaiQuyTrinhID);
        }
    }
    render() {
        var quyTrinhs = [];
        this.props.quyTrinhs.forEach(function (quyTrinh) {
            if (quyTrinh.loaiquytrinh == 1)
                quyTrinhs.push(
                    <QuyTrinhNhapRac key={quyTrinh.idquytrinh} quyTrinh={quyTrinh}></QuyTrinhNhapRac>
                );
            if (quyTrinh.loaiquytrinh == 2)
                quyTrinhs.push(
                    <QuyTrinhKhac key={quyTrinh.idquytrinh} quyTrinh={quyTrinh}></QuyTrinhKhac>
                );
            if (quyTrinh.loaiquytrinh == 3)
                quyTrinhs.push(
                    <QuyTrinhKhac key={quyTrinh.idquytrinh} quyTrinh={quyTrinh}></QuyTrinhKhac>
                );
        });
        return <Card style={this.props.style} >
            <Table>
                <TableHead>
                    <TableRow style={{ height: "20px" }}>
                        <TableCell colSpan={2} style={{ textAlign: "center" }}>
                            <Typography style={{ fontWeight: "bold" }} gutterBottom>
                                Quy trình {this.props.loaiQuyTrinh}
                                <a style={{ float: "right", display: this.props.displayLink ? "block" : "none" }} href="/quy-trinh">Xem tất cả >></a>
                            </Typography>
                        </TableCell>
                    </TableRow>
                </TableHead>
            </Table>
            <Scrollbars style={{ height: this.props.height }}
                onScrollFrame={this.onScroll}>
                <Table>
                    <TableBody>
                        {quyTrinhs}
                        <TableRow style={{ display: !this.props.displayLink||this.props.quyTrinhs.length==0 ? "none" : "table-row" }}>
                            <TableCell colSpan="3">
                                <CircularProgress style={{ display: "block", margin:"0 auto" }} size={24} />
                            </TableCell>
                        </TableRow>
                    </TableBody >

                </Table>
            </Scrollbars>

        </Card>;
    }
}
export default QuyTrinhView;
