import React, { Component } from 'react'
import { TableRow, TableCell, Typography, IconButton, TextField, Select, MenuItem, Checkbox } from '@material-ui/core';
import { Save, Delete } from '@material-ui/icons';
import { host } from '../../socket';
import axios from 'axios';
export default class Xe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            xe: props.xe,
            checked: false,
        }
        this.check = this.check.bind(this);
        this.xoaXe = this.xoaXe.bind(this);
        this.luuXe = this.luuXe.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    componentWillReceiveProps(props) {
        this.setState({
            xe: props.xe,
        });
    }
    check() {
        this.setState(prev => {
            return {
                checked: !prev.checked,
            };
        });
    }
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        var xe = this.state.xe;
        xe[name] = value;
        this.setState({
            xe: xe
        });
    }
    luuXe(xe) {
        var url = host + "/xe/luu";
        axios.post(url, xe)
            .then(res => {
                if (res.data.resCode == '-1') {
                    alert(res.data.resMessage);
                }
                else {
                    alert(res.data.resMessage);
                }
                this.props.refresh();
            });
    }
    xoaXe(xe) {
        var url = host + "/xe/xoa";
        axios.post(url, xe)
            .then(res => {
                if (res.data.resCode == '-1') {
                    alert(res.data.resMessage);
                }
                else {
                    alert(res.data.resMessage);
                }

                this.props.refresh();
            });
    }
    render() {
        return (
            <TableRow style={{ height: "20px" }}>
                <TableCell style={{ textAlign: "center" }}>
                    <Typography style={{ textAlign: "center" }} variant="h6" gutterBottom>
                        {this.state.xe.idXe}
                    </Typography>
                </TableCell>
                <TableCell style={{ textAlign: "center" }}>
                    <TextField name="bienso" value={this.state.xe.bienso} onChange={this.handleInputChange}>
                    </TextField>
                </TableCell>
                <TableCell style={{ textAlign: "center" }}>
                    <Select name="loaixe" variant="standard" value={this.state.xe.loaixe} onChange={this.handleInputChange}>
                        <MenuItem value="1">Xe nhập rác</MenuItem>
                        <MenuItem value="3">Xe chôn rác</MenuItem>
                        <MenuItem value="2">Xe cân ngoài</MenuItem>
                    </Select>
                </TableCell>
                <TableCell style={{ textAlign: "center" }}>
                    <Select
                        style={{ width: "100%" }}
                        id="donvi"
                        select
                        name="idDonVi"
                        value={this.state.xe.idDonVi}
                        onChange={this.handleInputChange}
                        margin="normal">
                        {this.props.donVis.map((donVi) => {
                            return (<MenuItem value={donVi.idDonVi}>{donVi.tenDonVi}</MenuItem>);
                        })}

                    </Select>
                </TableCell>
                
                <TableCell style={{ textAlign: "center", width:"150px" }}>
                    <TextField name="trongLuongKhongTai" value={this.state.xe.trongLuongKhongTai} onChange={this.handleInputChange}>
                    </TextField>
                </TableCell>
                <TableCell style={{ textAlign: "center" }}>
                    <IconButton >
                        <Save color="primary" onClick={() => { if (window.confirm("Bạn muốn thêm mới xe?")) this.luuXe(this.state.xe) }} />
                    </IconButton>
                    <IconButton>
                        <Delete color="error" onClick={() => { if (window.confirm("Bạn muốn xóa xe?")) this.xoaXe(this.state.xe) }} />
                    </IconButton>
                </TableCell>
            </TableRow>
        )
    }
}
