import React, { Component } from 'react'
import './dataTramSlot.css'
import AddIcon from '@material-ui/icons/AddCircleOutline';
import CancelIcon from '@material-ui/icons/Cancel';
import DataTramItem from '../dataTramItem/DataTramItem';

export default class DataTramSlot extends Component {
    render() {
        return (
            <div className="slot-container">
                {this.props.dataTram != null ? <div className="cancel-icon">
                    <CancelIcon onClick={() => this.props.removeDataTram(this.props.dataTram, this.props.selected)}></CancelIcon>
                </div> : ""}
                {this.props.dataTram == null ?
                    <AddIcon className="background-icon">
                    </AddIcon> : ""}
                {this.props.dataTram != null ? <DataTramItem  dataTram={this.props.dataTram}></DataTramItem> : ""}
                
            </div>
        )
    }
}
