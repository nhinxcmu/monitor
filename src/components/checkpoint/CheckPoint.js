import React, { Component } from 'react'
import './checkPoint.css'
import landfill from './landfill2.png'
import scale from './scale.png'

export default class CheckPoint extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        var place = "";
        var value = <div style={{ marginBottom: "0px", fontSize: "12px", marginLeft: "-5px" }}>
            <strong style={{ display: "flex", justifyItems: "center", width: "60px" }}>{this.props.value}</strong>
        </div>;
        if (this.props.place == "landfill") {
            place = landfill;
        }

        if (this.props.place == "scale") {
            place = scale;

        }


        return (
            <div style={{ display: "inline-block", width: "15px" }}>
                {value}
                <span className="dot" style={this.props.reached ? { backgroundColor: "limegreen" } : {}}></span>
                <img style={{ marginLeft: "-2px", marginBottom: "1px" }} width="20px" height="20px" src={place}>
                </img>
            </div>

        )
    }
}
