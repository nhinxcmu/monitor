import React, { Component } from 'react';

import MainDrawer from './components/mainDrawer/MainDrawer';
import { Typography, AppBar, IconButton, Toolbar, MenuItem, Menu, Icon } from '@material-ui/core/'
import { stompClient, ping, host } from './socket';
import MenuIcon from '@material-ui/icons/Menu';
import posed, { PoseGroup } from 'react-pose';
import { Route, Switch } from 'react-router-dom';
import AccountCircle from '@material-ui/icons/AccountCircle';


import './App.css'

import MainPage from './pages/mainPage/MainPage';
import DanhMucXePage from './pages/danhMuc/danhMucXe/DanhMucXePage';
import DanhMucVaiTroPage from './pages/danhMuc/danhMucVaiTro/DanhMucVaiTroPage';
import DataTramPage from './pages/dataTramPage/DataTramPage';
import QuyTrinhPage from './pages/quyTrinhPage/QuyTrinhPage';
import DonViPage from './pages/donViPage/DonViPage';
import BaoCaoPage from './pages/baoCaoPage/BaoCaoPage';
import NguoiDungPage from './pages/nguoiDungPage/NguoiDungPage';
import DoiMatKhau from './components/doiMatKhau/DoiMatKhau';

const RouteContainer = posed.div({
  enter: { opacity: 1, delay: 300, beforeChildren: true },
  exit: { opacity: 0 }
});
class App extends Component {
  constructor(props) {
    super(props);
    this.getPages = this.getPages.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.socketConnect = this.socketConnect.bind(this);
    this.state = {
      drawer: false,
      menuOpen: false,
      anchorEl: null,
      menuDoiMatKhau: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.menuDoiMatKhau = this.menuDoiMatKhau.bind(this);
    this.hideDoiMatKhauMenu = this.hideDoiMatKhauMenu.bind(this);
    this.socketConnect();
    ping();
    setInterval(ping, 300000);

  }
  handleClick(event) {
    this.setState({
      anchorEl: event.currentTarget,
    });
  }

  handleClose() {
    this.setState({
      anchorEl: null,
    });
  }
  menuDoiMatKhau() {
    this.setState({
      anchorEl: null,
      menuDoiMatKhau: true,
    });
  }
  dangXuat() {
    window.location.href = "/logout";
  }

  socketConnect() {
    stompClient.connect({}, () => {
      this.setState({});
    },
      (message) => {
        setTimeout(() => window.location.reload(), 10000);
        console.log('STOMP: Reconecting in 10 seconds');
      }
    );
  }
  hideDoiMatKhauMenu() {
    this.setState({
      menuDoiMatKhau: false
    });
  }






  toggleDrawer(open) {
    this.setState({
      drawer: open,
    });
  };
  getPages() {
    return (
      <Route render={({ location }) =>
        <div style={{ background: "#eeeeee", minHeight: "100vh" }}>
          <MainDrawer open={this.state.drawer} toggleDrawer={this.toggleDrawer} >

          </MainDrawer>
          <AppBar position="static" style={{ marginBottom: "10px" }}>
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu" onClick={() => {
                this.toggleDrawer(true);
              }}>
                <MenuIcon />
              </IconButton>
              <Typography variant="h4" style={{ padding: "15px", flexGrow: 1 }} color="inherit">
                Hệ Thống Giám Sát Nhà Máy Xử Lý Rác
              </Typography>
              <IconButton
                aria-owns={this.state.anchorEl ? 'simple-menu' : undefined}
                aria-haspopup="true"
                onClick={this.handleClick}
                style={{ float: "right" }}
                color="inherit">
                <AccountCircle style={{ fontSize: "25px" }} />
              </IconButton>
              <Menu id="simple-menu" anchorEl={this.state.anchorEl} open={Boolean(this.state.anchorEl)} onClose={this.handleClose}>
                <MenuItem onClick={this.menuDoiMatKhau}><div style={{marginRight:"5px"}}><Icon style={{fontSize:"25px"}}>lock_open</Icon></div><span style={{fontSize:"15px"}}>Đổi mật khẩu</span></MenuItem>
                <MenuItem onClick={this.dangXuat}><div style={{marginRight:"5px"}}><Icon style={{fontSize:"25px"}}>input</Icon></div><span style={{fontSize:"15px"}}>Đăng xuất</span></MenuItem>
              </Menu>
            </Toolbar>
          </AppBar>
          <PoseGroup>
            <RouteContainer key={location.key ? location.key : 1}>
              <Switch location={location}>
                <Route exact path="/" component={MainPage} key="Home" />
                <Route exact path="/giam-sat" component={MainPage} key="giam-sat" />
                <Route exact path="/xe" component={DanhMucXePage} key="xe" />
                <Route exact path="/vai-tro" component={DanhMucVaiTroPage} key="vai-tro" />
                <Route exact path="/data-tram" component={DataTramPage} key="data-tram" />
                <Route exact path="/quy-trinh" component={QuyTrinhPage} key="quy-trinh" />
                <Route exact path="/don-vi" component={DonViPage} key="don-vi" />
                <Route exact path="/bao-cao" component={BaoCaoPage} key="bao-cao" />
                <Route exact path="/nguoi-dung" component={NguoiDungPage} key="nguoi-dung" />
              </Switch>
            </RouteContainer>
          </PoseGroup>

        </div>
      }
      />);
  }
  render() {
    if (stompClient.connected)
      return (
        <div>
          {this.getPages()}
          <DoiMatKhau show={this.state.menuDoiMatKhau} onHide={this.hideDoiMatKhauMenu}></DoiMatKhau>
        </div>);
    return (<div></div>);
  }
}

export default App;
