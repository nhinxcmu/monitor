
import { errorCodes } from './socket';

var errorCode = function (code) {
    try {
        if (code > 0 && code != 7)
            return "Thiếu hình";
        return errorCodes.find(n => n.errorcode == code).value;
    }
    catch (e) {
        return "";
    }
}
var tram = function (code) {
    switch (code) {
        case 1:
            return "Cân";
        case 2:
            return "BTK";

        default:
            return "";
    }
}
export  { errorCode, tram } 